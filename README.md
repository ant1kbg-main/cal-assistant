Calendar Assistant is a graphical interface design to help you to keep track of any cycling event, or any expiration date (e.g. expiration of administrative documents, etc.). 

It is mainly written in Python, and GTK.

![](screenshot.png)

**Installation**

<details><summary>Debian (and its descendant)</summary>

You need to be root to execute the following code

`# ./tools/generate_deb.sh`

Then, still in root: 

`# apt install ./packages/cal-assistant-deb.deb`

or, if you did modification on the code after a first Installation

`# apt reinstall ./packages/cal-assistant-deb.deb`

After installation, the application should appear as any other application in your menus/dock/etc.
</details>

**Launch it without installation**

`$ ./src/__init__.py`

The application would copy some files to _~/.local/cal-assistant_.

**It is recommended to back-up that folder (mainly the _ca.xml_ file) time to time (would be done automatically in a future release).**

**Technical details**

```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Glade                            5              0             45           4728
Python                          17            369            121           2114
PO File                          2            332            382            830
Visualforce Page                14             14              0            359
XML                             10              0              0            188
Bourne Shell                     6             25              7            130
CSS                              1             27             42             25
-------------------------------------------------------------------------------
SUM:                            55            767            597           8374
-------------------------------------------------------------------------------
```

