"""
The application module manage all aspected related to the application:
 - main menu
 - shortcuts
 - link between the different windows
"""

import os
from pathlib import Path
import gi
gi.require_version("Gtk", "3.0")
#pylint: disable=wrong-import-position
from gi.repository import GLib, Gtk, Gdk, Gio
from CalendarAssistant import CalendarAssistant
from MainWindow import (
    MainWindow,
    WINDOW,
    BTN_ACTIVATE, BTN_DEACTIVATE, BTN_RENEW, BTN_CANCEL_RENEW)
from EditCategoriesWindow import EditCategoriesWindow
from EditEventWindow import EditEventWindow
from Configuration import (
    cfg_get_bool, cfg_set, get_local_file,
    UI, BG,
    SHOW_EMPTY_CATEGORIES, SHOW_CONFIRMATION_RENEW, DIRECT_ACCESS_EVENT_EDITION,
    CAL_FILE_PER_CATEGORY, NOTIFICATION_ON_D_DAY)
from Translation import _
from TextManager import xstr
from LogManager import logger
import DataFileDefinition as DFD
import TodoFileDefinition as TFD
#pylint: enable=wrong-import-position

APPLICATION_ID = 'org.ant1kbg.cal-assistant'

### UI objects ###

FILE_MAIN_UI = 'UI/main.glade'
FILE_CSS = 'UI/style.css'

class Application(Gtk.Application):

    def __init__(self):
        logger.debug(locals())
        Gtk.Application.__init__(self, application_id=APPLICATION_ID)

        self.backend = CalendarAssistant()
        self.main_window = None
        self.edit_categories_window = None
        self.edit_event_window = None

        self.menu = None
        self.current_menu_section = None

        self.preferences = {}
        self.shortcut_groups = {}
        self.my_accelerators = Gtk.AccelGroup()

        self.gtk_about_dialog = None
        self.gtk_settings_window = None
        self.gtk_shortcuts_window = None

    ### Main Menu ###

    def create_menu(self):
        logger.debug(locals())
        self.menu = Gio.Menu()
        self.current_menu_section = Gio.Menu()

    def add_menu_section(self, last=False):
        logger.debug(locals())
        self.menu.append_section(None, self.current_menu_section)
        if last:
            self.set_app_menu(self.menu)
        else:
            self.current_menu_section = Gio.Menu()

    def add_menu_entry(self, title, action_title, call):
        logger.debug(locals())
        self.current_menu_section.append(_(title.title()), action_title)
        action = Gio.SimpleAction.new(title, None)
        action.connect('activate', call)
        self.add_action(action)

    ### Shortcuts ###

    def add_shortcut(self, group, accelerator, title, widget, action):
        if not group in self.shortcut_groups:
            self.shortcut_groups[group] = Gtk.ShortcutsGroup()
            self.shortcut_groups[group].set_property('title', _(group))
        shortcut = Gtk.ShortcutsShortcut()
        shortcut.set_property('accelerator', accelerator)
        shortcut.set_property('title', title)
        self.shortcut_groups[group].add(shortcut)
        if widget is not None:
            key, mod = Gtk.accelerator_parse(accelerator)
            widget.add_accelerator('clicked', self.my_accelerators, key,
                                   mod, Gtk.AccelFlags.VISIBLE)
        if action is not None:
            self.add_accelerator(accelerator, action, None)

    ### Main functions ###

    def do_startup(self):
        logger.debug(locals())

        Gtk.Application.do_startup(self)
        GLib.set_application_name(_('Calendar Assistant'))
        GLib.set_prgname(APPLICATION_ID)

        self.create_menu()
        self.add_menu_entry('settings', 'app.settings',
                            self.show_settings)
        self.add_menu_section()
        self.add_menu_entry('shortcuts', 'app.shortcuts',
                            self.show_shortcuts)
        self.add_menu_entry('help', 'app.help', self.show_help)
        self.add_menu_entry('about', 'app.about', self.show_about)
        self.add_menu_entry('quit', 'app.quit', self.quit_app)
        self.add_menu_section(True)

    def do_activate(self):
        logger.debug(locals())

        if self.main_window is None:
            builder = Gtk.Builder()
            builder.add_from_file(FILE_MAIN_UI)

            # Apply CSS #
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(FILE_CSS)
            Gtk.StyleContext.add_provider_for_screen(screen, provider,
                                                     Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

            self.main_window = MainWindow(self.backend, self)
            self.edit_categories_window = EditCategoriesWindow(
                self.backend, self)
            self.edit_event_window = EditEventWindow(self.backend, self)

            self.gtk_about_dialog = builder.get_object('about_dialog')
            self.gtk_settings_window = builder.get_object('settings')
            self.gtk_shortcuts_window = Gtk.ShortcutsWindow()
            self.gtk_shortcuts_window.connect('delete-event',
                                              self.hide_shortcuts)

            self.add_shortcut('General', '<Ctrl>Q', _('Quit'), None, 'app.quit')
            self.add_shortcut('General', 'Escape', _('Close modal windows'), None, None)
            self.add_shortcut('General', 'F1', _('Help'), None, 'app.help')
            self.add_shortcut('Main Window', '<Ctrl>space', _('Activate expiration'),
                              self.main_window.UI(BTN_ACTIVATE), None)
            self.add_shortcut('Main Window', 'Delete', _('Deactivate expiration'),
                              self.main_window.UI(BTN_DEACTIVATE), None)
            self.add_shortcut('Main Window', '<Ctrl>Return', _('Renew expiration'),
                              self.main_window.UI(BTN_RENEW), None)
            self.add_shortcut('Main Window', '<Ctrl><Shift>Return', _('Cancel renewing expiration'),
                              self.main_window.UI(BTN_CANCEL_RENEW), None)
            self.main_window.UI(WINDOW).add_accel_group(self.my_accelerators)
            main_shortcuts_section = Gtk.ShortcutsSection()
            for sc_g in self.shortcut_groups:
                main_shortcuts_section.add(self.shortcut_groups[sc_g])
            main_shortcuts_section.show()
            self.gtk_shortcuts_window.add(main_shortcuts_section)

            builder.connect_signals(self)

            self.preferences[builder.get_object('cfg_show_empty_categories')] \
                = [UI, SHOW_EMPTY_CATEGORIES]
            self.preferences[builder.get_object('cfg_show_confirmation_renew')] \
                = [UI, SHOW_CONFIRMATION_RENEW]
            self.preferences[builder.get_object('cfg_direct_access_edit')] \
                = [UI, DIRECT_ACCESS_EVENT_EDITION]
            self.preferences[builder.get_object('cfg_generate_cal_each_category')] \
                = [BG, CAL_FILE_PER_CATEGORY]
            self.preferences[builder.get_object('cfg_notification_d_day')] \
                = [BG, NOTIFICATION_ON_D_DAY]

            self.show_config()

            self.main_window.open()
            self.main_window.update_main_store()

        if os.path.isfile(get_local_file(TFD.FILENAME)):
            with open(get_local_file(TFD.FILENAME)) as todo_file:
                f_content = todo_file.readline()
                action, path = f_content.rstrip().split(TFD.SEPARATOR)
                self.main_window.select(path)
                if action == TFD.RENEW:
                    self.main_window.s__renew(None)
            os.remove(get_local_file(TFD.FILENAME))

    def show_config(self):
        for widget in self.preferences:
            conf_attributes = self.preferences[widget]
            widget.set_active(cfg_get_bool(conf_attributes[1], conf_attributes[0]))

    ### Main signals ###

    def s__switch_config(self, widget, position):
        logger.info(locals())
        if widget in self.preferences:
            conf_attributes = self.preferences[widget]
            cfg_set(conf_attributes[1], position, conf_attributes[0])
        else:
            logger.error('Could not find widget in config table')

    def show_settings(self, action, parameter):
        logger.debug(locals())
        self.gtk_settings_window.run()
        self.gtk_settings_window.hide()
        self.main_window.update_main_store()

    def show_shortcuts(self, action, parameter):
        logger.debug(locals())
        self.gtk_shortcuts_window.show_all()

    def hide_shortcuts(self, action, parameter):
        logger.debug(locals())
        self.gtk_shortcuts_window.hide()
        return True

    def show_help(self, action, parameter):
        logger.debug(locals())
        for lang in GLib.get_language_names():
            if os.path.exists(os.path.join('doc', lang)):
                path = 'ghelp:'+str(Path(os.path.join('doc', lang, xstr(parameter))).resolve())
                Gtk.show_uri(None, path, 0)
                return

    def show_about(self, action, parameter):
        logger.debug(locals())
        self.gtk_about_dialog.run()
        self.gtk_about_dialog.hide()

    def quit_app(self, action, parameter):
        logger.debug(locals())
        self.main_window.close()
        self.quit()

    ### Other windows ###

    def create_new_event(self, path):
        logger.debug(locals())
        response = self.edit_event_window.load_window(None)
        if response == Gtk.ResponseType.OK:
            new_id = self.backend.add(DFD.EVENT, path)
            new_path = DFD.PATH_SEPARATOR.join([path, new_id])
            event = self.backend.get(DFD.EVENT, new_path)
            for attr in [DFD.LABEL, DFD.EVENT_DESCRIPTION,
                         DFD.EVENT_APPROACH, DFD.EVENT_DURATION,
                         DFD.EVENT_EXPIRING, DFD.EVENT_NOTIFICATION]:
                self.backend.update_obj_attr(event, attr, self.edit_event_window.get(attr))
            return new_id, self.edit_event_window.get(DFD.LABEL)
        return None, None

    def edit_event(self, path, update=False):
        logger.debug(locals())
        event = self.backend.get(DFD.EVENT, path)
        response = self.edit_event_window.load_window(event)
        if response == Gtk.ResponseType.OK:
            for attr in [DFD.LABEL, DFD.EVENT_DESCRIPTION, DFD.EVENT_APPROACH, DFD.EVENT_DURATION,
                         DFD.EVENT_EXPIRING, DFD.EVENT_NOTIFICATION]:
                self.backend.update_obj_attr(event, attr, self.edit_event_window.get(attr))
            if update:
                self.backend.save()
            return self.edit_event_window.get(DFD.LABEL)
        return None

    def open_dialog_categories(self, button):
        logger.debug(locals())
        self.edit_categories_window.load_window()
