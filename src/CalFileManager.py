import os
from Configuration import cfg_get_bool, get_local_file, CAL_FILE_PER_CATEGORY, BG
from TextManager import random_id
from DateManager import str2ics_date, str2ics_next_date
from Translation import l_
from LogManager import logger
import DataFileDefinition as DFD

DIR_CAL = get_local_file('cal')
EXT = '.ics'
MAIN_FILE = 'main'

def create_ics_file(filename, array, ca):
    logger.debug(locals())
    f = open(filename, 'w')
    f.write('BEGIN:VCALENDAR\n')
    f.write('VERSION:2.0\n')
    f.write('PRODID:-//ant1//CalendarAssistant//EN\n')
    for expiration in array:
        if expiration.get(DFD.EXPIRATION_EXPIRE) == DFD.EXPIRE_NA or expiration.get(DFD.EXPIRATION_ACTIVE) == DFD.ACTIVE_NO:
            continue
        eid = expiration.get(DFD.ID[DFD.EXPIRATION])
        instance = ca.get_parent(expiration)
        category = ca.get_parent(instance)
        path = DFD.PATH_SEPARATOR.join([category.get(DFD.ID[DFD.CATEGORY]), instance.get(DFD.ID[DFD.INSTANCE]), eid])
        event = ca.get(DFD.EVENT, path)
        label = l_(event.get(DFD.LABEL))
        if len(category.findall(DFD.INSTANCE)) > 1:
            label = '[' + l_(instance.get(DFD.LABEL)) + '] ' + label
        f.write('BEGIN:VTODO\n')
        f.write('UID:'+random_id()+'\n')
        f.write('DTSTAMP:'+str2ics_date(expiration.get(DFD.EXPIRATION_EXPIRE))+'T120000Z\n')
        f.write('DTSTART;VALUE=DATE:'+str2ics_date(expiration.get(DFD.EXPIRATION_EXPIRE))+'\n')
        f.write('DTEND;VALUE=DATE:'+str2ics_next_date(expiration.get(DFD.EXPIRATION_EXPIRE))+'\n')
        f.write('SUMMARY:'+label+'\n')
        f.write('DESCRIPTION:'+label+'\n')
        f.write('END:VTODO\n')
    f.write('END:VCALENDAR\n')
    f.close()

def update_all_cals(ca):
    logger.debug(locals())
    existing_files = os.listdir(DIR_CAL)
    for filename in existing_files:
        if filename.endswith(EXT):
            os.remove(os.path.join(DIR_CAL, filename))
    if cfg_get_bool(CAL_FILE_PER_CATEGORY, BG):
        categories = ca.get_sorted_array(DFD.CATEGORY)
        for category in categories:
            create_ics_file(os.path.join(DIR_CAL, category.get(DFD.LABEL)+EXT), ca.get_sorted_array(DFD.EXPIRATION, category.get(DFD.ID[DFD.CATEGORY]), True), ca)
    else:
        create_ics_file(os.path.join(DIR_CAL, MAIN_FILE+EXT), ca.get_sorted_array(DFD.EXPIRATION, None, True), ca)
