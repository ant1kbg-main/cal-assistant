import xml.etree.ElementTree as ET
import re
from DateManager import date2str, str2date, today, str2delta, get_age
from Configuration import get_local_file, DATA_FILENAME
from LogManager import logger
from TextManager import random_id
from Translation import l_
import DataFileDefinition as DFD

FILENAME = get_local_file(DATA_FILENAME)

class CalendarAssistant(object):

    def __init__(self):
        logger.debug(locals())
        self.tree = ET.ElementTree()
        self.root = None
        self.load()

    ### Main methods ###

    def load(self):
        logger.debug(locals())
        self.root = self.tree.parse(FILENAME)

    def save(self):
        logger.debug(locals())
        self.tree.write(FILENAME)

    def cancel_changes(self):
        logger.debug(locals())
        self.load()

    def rename(self, obj_type, path, new_name):
        logger.debug(locals())
        self.get(obj_type, path).attrib[DFD.LABEL] = new_name

    def update_attr(self, obj_type, path, attribute, value):
        logger.debug(locals())
        self.update_obj_attr(self.get(obj_type, path), attribute, value)

    def update_obj_attr(self, obj, attribute, value):
        logger.debug(locals())
        if attribute in obj.attrib:
            if l_(obj.attrib[attribute]) == value:
                return
        obj.attrib[attribute] = value

    def update_value(self, obj_type, path, value):
        logger.debug(locals())
        self.get(obj_type, path).text = str(value)

    def remove(self, obj_type, path):
        logger.debug(locals())
        to_remove = self.get(obj_type, path)
        self.get_parent(to_remove).remove(to_remove)

    def add(self, obj_type, path=None, nid=None):
        logger.debug(locals())
        new_id = random_id() if nid is None else nid
        new_obj = ET.Element(obj_type)
        new_obj.attrib[DFD.ID[obj_type]] = new_id
        if obj_type == DFD.CATEGORY and path is None:
            new_obj.attrib[DFD.POSITION] = str(len(self.root.findall(DFD.CATEGORY)))
            first_instance = ET.Element(DFD.INSTANCE)
            first_instance.attrib[DFD.ID[DFD.INSTANCE]] = 'default'
            first_instance.attrib[DFD.POSITION] = '0'
            first_instance.attrib[DFD.LABEL] = l_('_Default')
            new_obj.append(first_instance)
            self.root.append(new_obj)
        elif obj_type in [DFD.PARAMETER, DFD.INSTANCE, DFD.EVENT] and path is not None:
            if obj_type == DFD.INSTANCE:
                new_obj.attrib[DFD.POSITION] = str(len(self.get(DFD.CATEGORY, path).findall(DFD.INSTANCE)))
                for parameter in self.get(DFD.CATEGORY, path).findall(DFD.PARAMETER):
                    new_param = ET.Element(DFD.PARAM)
                    new_param.attrib[DFD.ID[DFD.PARAM]] = parameter.get(DFD.ID[DFD.PARAMETER])
                    logger.debug('> Adding new param '+str(new_param)+' in '+str(new_obj))
                    new_obj.append(new_param)
                for event in self.get(DFD.CATEGORY, path).findall(DFD.EVENT):
                    new_exp = ET.Element(DFD.EXPIRATION)
                    new_exp.attrib[DFD.ID[DFD.EXPIRATION]] = event.get(DFD.ID[DFD.EVENT])
                    new_exp.attrib[DFD.EXPIRATION_ACTIVE] = DFD.ACTIVE_NO
                    new_exp.attrib[DFD.EXPIRATION_EXPIRE] = DFD.EXPIRE_NA
                    logger.debug('> Adding a new exp '+str(new_exp)+' in '+str(new_obj))
                    new_obj.append(new_exp)
            elif obj_type == DFD.EVENT:
                for instance in self.get(DFD.CATEGORY, path).findall(DFD.INSTANCE):
                    new_exp = ET.Element(DFD.EXPIRATION)
                    new_exp.attrib[DFD.ID[DFD.EXPIRATION]] = new_id
                    new_exp.attrib[DFD.EXPIRATION_ACTIVE] = DFD.ACTIVE_NO
                    new_exp.attrib[DFD.EXPIRATION_EXPIRE] = DFD.EXPIRE_NA
                    logger.debug('> Adding new exp '+str(new_exp)+' in '+str(instance))
                    instance.append(new_exp)
            elif obj_type == DFD.PARAMETER:
                for instance in self.get(DFD.CATEGORY, path).findall(DFD.INSTANCE):
                    new_param = ET.Element(DFD.PARAM)
                    new_param.attrib[DFD.ID[DFD.PARAM]] = new_id
                    logger.debug('> Adding new param '+str(new_param)+' in '+str(instance))
                    instance.append(new_param)
            self.get(DFD.CATEGORY, path).append(new_obj)
        else:
            logger.error('Cannot add object')
            logger.error(locals())
        return new_id

    def reorder(self, obj_type, ordered_ids):
        logger.error('Not implemented yet')
        logger.debug(self)

    def import_xml(self, filename):
        logger.debug(locals())
        try:
            new_tree = ET.ElementTree()
            new_tree_root = new_tree.parse(filename)
        except:
            return DFD.IMPORT_WRONG_FILE_FORMAT
        imported_something = False
        i = len(self.root.findall(DFD.CATEGORY))
        for category in new_tree_root.findall(DFD.CATEGORY):
            cid = category.get(DFD.ID[DFD.CATEGORY])
            if self.get(DFD.CATEGORY, cid) is None:
                self.root.append(category)
                category.attrib[DFD.POSITION] = str(i)
                imported_something = True
                i = i + 1
        if imported_something:
            return DFD.IMPORT_OK
        return DFD.IMPORT_NOTHING_TO_IMPORT

    ### Accessing the data ###

    def get_parent(self, obj):
        logger.debug(locals())
        return {c:p for p in self.tree.iter() for c in p}[obj]

    def get(self, obj_type, path):
        logger.debug(locals())
        path_array = path.split(DFD.PATH_SEPARATOR)
        if obj_type == DFD.CATEGORY and len(path_array) == 1:
            return self.root.find(DFD.CATEGORY + '[@'+DFD.ID[obj_type] + '="' + path_array[0]+'"]')
        if obj_type in [DFD.PARAMETER, DFD.EVENT, DFD.INSTANCE] and len(path_array) == 2:
            return self.root.find(DFD.CATEGORY + '[@'+DFD.ID[DFD.CATEGORY] + '="'
                                  + path_array[0] + '"]/' + obj_type + '[@'+DFD.ID[obj_type] + '="'
                                  + path_array[1] + '"]')
        if obj_type in [DFD.PARAM, DFD.EXPIRATION] and len(path_array) == 3:
            return self.root.find(DFD.CATEGORY + '[@' + DFD.ID[DFD.CATEGORY] + '="' + path_array[0]
                                  + '"]/' + DFD.INSTANCE + '[@' + DFD.ID[DFD.INSTANCE] + '="'
                                  + path_array[1] + '"]/' + obj_type + '[@' + DFD.ID[obj_type]
                                  + '="' + path_array[2] + '"]')
        if obj_type == DFD.EVENT and len(path_array) == 3:
            return self.root.find(DFD.CATEGORY + '[@' + DFD.ID[DFD.CATEGORY] + '="' + path_array[0]
                                  + '"]/' + obj_type + '[@' + DFD.ID[obj_type] + '="'
                                  + path_array[2] + '"]')
        if obj_type == DFD.PARAM and len(path_array) == 4:
            return self.root.find(DFD.CATEGORY + '[@' + DFD.ID[DFD.CATEGORY] + '="' + path_array[0]
                                  + '"]/' + DFD.INSTANCE + '[@' + DFD.ID[DFD.INSTANCE] + '="'
                                  + path_array[1] + '"]/' + obj_type + '[@' + DFD.ID[obj_type]
                                  + '="' + path_array[3] + '"]')
        logger.error('Could not get object')
        logger.error(locals())
        return None

    def get_sorted_array(self, obj_type, path=None, skip_nodes=False, reverse=False):
        logger.debug(locals())
        if path is None:
            from_node = self.root
        elif obj_type in [DFD.INSTANCE, DFD.EVENT, DFD.EXPIRATION] and path is not None:
            from_node = self.get(DFD.CATEGORY, path)
        else:
            logger.error('Could not find the parent object')
            logger.error(locals())
            return None
        if skip_nodes:
            array = [elem for elem in from_node.iter() if elem.tag == obj_type]
        else:
            array = from_node.findall(obj_type)
        rev = DFD.OK_REVERSE[obj_type]
        if rev is None:
            rev = reverse
        return sorted(array, key=DFD.OK[obj_type], reverse=rev)

    def obj2path(self, obj):
        logger.debug(locals())
        path = obj.get(DFD.ID[obj.tag])
        parent = self.get_parent(obj)
        while parent != self.root:
            path = DFD.PATH_SEPARATOR.join([parent.get(DFD.ID[parent.tag]), path])
            parent = self.get_parent(parent)
        return path

    # Event information #

    def get_conditional_attribute(self, path, param_name):
        logger.debug(locals())
        event = self.get(DFD.EVENT, path)
        cases = event.get(param_name).split(DFD.CASES_SEPARATOR)
        n = len(cases)
        if n == 1:
            return cases[0]
        for i in range(n-1):
            condition, case = cases[i].split(DFD.CONDITION_SEPARATOR)
            if self.conditions_fulfilled(path, condition):
                return case
        return cases[n-1]

    # Expiration information #

    def get_current_approach(self, path):
        logger.debug(locals())
        return self.get_conditional_attribute(path, DFD.EVENT_APPROACH)

    def get_current_duration(self, path):
        logger.debug(locals())
        return str2delta(self.get_conditional_attribute(path, DFD.EVENT_DURATION))

    def get_current_expiring(self, path):
        logger.debug(locals())
        return self.get_conditional_attribute(path, DFD.EVENT_EXPIRING)

    def is_expiration_not_set(self, exp):
        logger.debug(locals())
        return exp.get(DFD.EXPIRATION_EXPIRE) == DFD.EXPIRE_NA

    def is_expiration_active(self, exp):
        logger.debug(locals())
        return exp.get(DFD.EXPIRATION_ACTIVE) == DFD.ACTIVE_YES

    def is_expiration_expiring(self, exp):
        logger.debug(locals())
        instance = self.get_parent(exp)
        category = self.get_parent(instance)
        path = DFD.PATH_SEPARATOR.join([category.get(DFD.ID[DFD.CATEGORY]),
                                        instance.get(DFD.ID[DFD.INSTANCE]),
                                        exp.get(DFD.ID[DFD.EXPIRATION])])
        return self.is_expiring(path)

    def is_active(self, path):
        logger.debug(locals())
        return self.is_expiration_active(self.get(DFD.EXPIRATION, path))

    def is_expiring(self, path):
        logger.debug(locals())
        return str2date(self.get(DFD.EXPIRATION, path).get(DFD.EXPIRATION_EXPIRE)) \
            <= today + str2delta(self.get_current_expiring(path))

    def is_renewable(self, path):
        logger.debug(locals())
        if self.get_current_approach(path) == DFD.APPROACH_ONCE:
            return True
        expiration = self.get(DFD.EXPIRATION, path)
        return str2date(self.get(DFD.EXPIRATION, path).get(DFD.EXPIRATION_EXPIRE)) \
            < today + self.get_current_duration(path)

    ### Computation ###

    def conditions_valid(self, path, condition):
        logger.debug(locals())
        if condition in [DFD.CONDITION_EXPIRED, DFD.CONDITION_NOT_EXPIRED]:
            return True
        variable_name, comparison_sign, value = condition.split(DFD.SPACE)
        if variable_name == DFD.VARIABLE_AGE:
            return self.get(DFD.PARAM, path + DFD.PATH_SEPARATOR
                            + DFD.PARAMETER_TYPE_YEAR_BIRTH) is not None
        if variable_name == DFD.LABEL:
            return True
        logger.error('Unknown variable : ' + variable_name)
        return False

    def conditions_fulfilled(self, path, condition):
        logger.debug(locals())
        event = self.get(DFD.EVENT, path)
        expiration = self.get(DFD.EXPIRATION, path)
        expiration_date = str2date(expiration.get(DFD.EXPIRATION_EXPIRE))
        instance = self.get_parent(expiration)
        if condition == DFD.CONDITION_EXPIRED:
            return expiration_date > today
        if condition == DFD.CONDITION_NOT_EXPIRED:
            return expiration_date <= today
        variable_name, comparison_sign, value = condition.split(DFD.SPACE)
        if variable_name == DFD.VARIABLE_AGE:
            if self.get(DFD.PARAM,
                        DFD.PATH_SEPARATOR.join([path, DFD.PARAMETER_TYPE_YEAR_BIRTH])) is None:
                logger.warning('Testing an invalid condition')
                return False
            year_path = DFD.PATH_SEPARATOR.join([path, DFD.PARAMETER_TYPE_YEAR_BIRTH])
            year = self.get(DFD.PARAM, year_path).text
            left_value = get_age(year)
            right_value = int(value)
        elif variable_name == DFD.LABEL:
            left_value = instance.get(DFD.LABEL)
            right_value = value
        else:
            logger.error('Unknown variable : ' + variable_name)
            return False
        if comparison_sign == '=':
            return left_value == right_value
        if comparison_sign == '!=':
            return left_value != right_value
        if comparison_sign == '<':
            return left_value < right_value
        if comparison_sign == '<=':
            return left_value <= right_value
        if comparison_sign == '>':
            return left_value > right_value
        if comparison_sign == '>=':
            return left_value >= right_value
        if comparison_sign == 'in':
            return left_value in right_value.split(',')
        if comparison_sign == 'like':
            matcher = re.compile(right_value.replace('%', '.*').replace('_', '.'))
            return matcher.match(left_value)
        logger.error('Unknown sign : ' + comparison_sign)
        return False

    def compute_next_date(self, path):
        logger.debug(locals())
        expiration = self.get(DFD.EXPIRATION, path)
        approach = self.get_current_approach(path)
        if approach == DFD.APPROACH_ONCE:
            return None
        rdelta = self.get_current_duration(path)
        if approach == DFD.APPROACH_FIXED:
            new_expire = str2date(expiration.get(DFD.EXPIRATION_EXPIRE)) + rdelta
            while new_expire <= today:
                new_expire += rdelta
            return new_expire
        if approach == DFD.APPROACH_FLEXIBLE:
            return today + rdelta
        logger.error('Unknown approach type : ' + approach)
        return None

    ### Update functions ###
    #
    # /!\ Update means a change is automatically saved (does not require confirmation)

    def renew(self, path):
        logger.debug(locals())
        next_date = self.compute_next_date(path)
        if next_date is None: #The expiration should become inactive
            self.deactivate(path)
        else:
            self.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_EXPIRE, date2str(next_date))
        self.save()

    def set_new_date(self, path, date):
        logger.debug(locals())
        self.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_EXPIRE, date2str(date))
        self.save()

    def activate(self, path):
        logger.debug(locals())
        self.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_ACTIVE, DFD.ACTIVE_YES)
        self.save()

    def deactivate(self, path):
        logger.debug(locals())
        self.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_ACTIVE, DFD.ACTIVE_NO)
        self.save()

    ### Edit functions ###
    #
    # /!\ Edit means a change may be wether saved or discard

    #TODO : is this used ???
    def set_category_position(self, cid, position):
        logger.debug(locals())
        self.update_attr(DFD.CATEGORY, cid, DFD.POSITION, position)

    #TODO : is this used ???
    def edit_event(self, path, label, expiring, notification, approach, duration, description):
        logger.debug(locals())
        event = self.get(DFD.EVENT, path)
        event.attrib[DFD.EVENT_EXPIRING] = expiring
        event.attrib[DFD.EVENT_NOTIFICATION] = notification
        event.attrib[DFD.EVENT_APPROACH] = approach
        event.attrib[DFD.EVENT_DURATION] = duration
        event.attrib[DFD.EVENT_DESCRIPTION] = description
        self.save()

    def set_yob_param(self, path, has_parameter):
        logger.debug(locals())
        category = self.get(DFD.CATEGORY, path)
        parameter = category.find(DFD.PARAMETER + '[@' + DFD.ID[DFD.PARAMETER] + '="'
                                  + DFD.PARAMETER_TYPE_YEAR_BIRTH + '"]')
        if has_parameter:
            if parameter is None:
                new_id = self.add(DFD.PARAMETER, path, DFD.PARAMETER_TYPE_YEAR_BIRTH)
                self.update_attr(DFD.PARAMETER, DFD.PATH_SEPARATOR.join([path, new_id]),
                                 DFD.PARAMETER_TYPE, 'integer')
                self.update_attr(DFD.PARAMETER, DFD.PATH_SEPARATOR.join([path, new_id]),
                                 DFD.LABEL, '_Year of birth')
        else:
            if parameter is not None:
                self.remove(DFD.PARAMETER, path + DFD.PATH_SEPARATOR + parameter.get(DFD.ID[DFD.PARAMETER]))
