import os
import configparser
import shutil

CONFIG_FILENAME = 'configuration.cfg'
DATA_FILENAME = 'ca.xml'
LOCAL_DIR = os.path.join(os.path.expanduser('~'), '.local', 'share', 'cal-assistant')
LOCAL_CONFIG_FILENAME = os.path.join(LOCAL_DIR, CONFIG_FILENAME)

### DATES ###

SYSTEM_DATE_FORMAT = '%Y-%m-%d'
LOCAL_DATE_FORMAT = '%d %B %Y'
ICS_DATE_FORMAT = '%Y%m%d'

### BOOL ###

TRUE = 'True'
FALSE = 'False'

### CONFIG FILE ###

UI = 'UI' ### UI ###
INTERNAL = 'INTERNAL' 

SHOW_EMPTY_CATEGORIES = 'show_empty_categories'
SHOW_CONFIRMATION_RENEW = 'show_confirmation_renew'
DIRECT_ACCESS_EVENT_EDITION = 'direct_access_to_event_edition'
SLIDE_POSITION = 'slide_position'
HEIGHT = 'height'
WIDTH = 'width'
ORDER_ASCENDING = 'order_ascending'
GRP_BY_CATEGORIES = 'group_by_categories'
SHOW_NOT_URGENT = 'show_not_urgent'
SHOW_NOT_ACTIVE = 'show_not_active'

BG = 'BG' ### BG ###

CAL_FILE_PER_CATEGORY = 'cal_file_per_category'
NOTIFICATION_ON_D_DAY = 'notification_on_d_day'

cfg = configparser.ConfigParser()

def get_local_file(filename):
    return os.path.join(LOCAL_DIR, filename)

def cfg_get_bool(property_name, section_name=UI):
    return cfg[section_name].getboolean(property_name)

def cfg_get_int(property_name, section_name=UI):
    return cfg[section_name].getint(property_name)

def cfg_get_float(property_name, section_name=UI):
    return cfg[section_name].getint(property_name)

def cfg_get_str(property_name, section_name=UI):
    return cfg[section_name][property_name]

def cfg_set(property_name, value, section_name=UI):
    if type(value) == bool:
        val = TRUE if value else FALSE
    else:
        val = str(value)
    cfg[section_name][property_name] = val
    with open(LOCAL_CONFIG_FILENAME, 'w') as configfile:
        cfg.write(configfile)

def cfg_switch(property_name, section_name=UI):
    cfg_set(property_name, not cfg_get_bool(property_name, section_name), section_name)
   
def local_dir_exists():
    return os.path.exists(LOCAL_DIR)

def create_local_dir():
    if not local_dir_exists():
        os.mkdir(LOCAL_DIR)
        os.mkdir(os.path.join(LOCAL_DIR, 'cal'))
        shutil.copy(CONFIG_FILENAME, LOCAL_DIR)
        shutil.copy(DATA_FILENAME, LOCAL_DIR)
        shutil.copytree('icons', os.path.join(LOCAL_DIR, 'icons'))

if local_dir_exists():
    cfg.read(LOCAL_CONFIG_FILENAME)
else:
    cfg.read(CONFIG_FILENAME)

