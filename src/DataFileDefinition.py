"""
This file define the Data file (containing all data).
The data file is an XML file. We'll find below :
 - the tags definitions (tag names)
 - the attributes definitions (attribute names)
 - for enums, the possible values
 - the few signs used in values or attributes
 - the keys used to order the data
"""

from DateManager import str2date as s2d

### TAGS ###
ROOT = 'ca'
# >
CATEGORY = 'category'
# >>
PARAMETER = 'parameter'
EVENT = 'event'
INSTANCE = 'instance'
# >>>
PARAM = 'param'
EXPIRATION = 'expiration'

### IDs ###
ID = {}
ID[CATEGORY] = 'cid'
ID[PARAMETER] = 'pid'
ID[EVENT] = 'eid'
ID[INSTANCE] = 'iid'
ID[PARAM] = 'pid'
ID[EXPIRATION] = 'eid'

### ATTRIBUTES ###

# Common attributes #
LABEL = 'label'

PARAMETER_TYPE = 'type'

EVENT_DESCRIPTION = 'description'
EVENT_APPROACH = 'approach'
EVENT_DURATION = 'duration'
EVENT_EXPIRING = 'expiring'
EVENT_NOTIFICATION = 'notification'

EXPIRATION_ACTIVE = 'active'
EXPIRATION_EXPIRE = 'expire'

### Possible values ###

APPROACH_ONCE = 'once'
# Fixed means the next expiration date is based on the current
# expiration date
APPROACH_FIXED = 'fixed'
# Flexible means the next expiration date is based on the day when the
# event if renewed
APPROACH_FLEXIBLE = 'flexible'

CONDITION_EXPIRED = 'expired'
CONDITION_NOT_EXPIRED = 'not expired'

VARIABLE_AGE = 'age'

PARAMETER_TYPE_YEAR_BIRTH = 'year_birth'

ACTIVE_YES = 'Y'
ACTIVE_NO = 'N'

EXPIRE_NA = ''

POSITION = 'i'

### Order Key ###
OK = {}
OK[CATEGORY] = lambda x: (x.get(POSITION))
OK[INSTANCE] = lambda x: (x.get(POSITION))
OK[EVENT] = lambda x: (x.get(LABEL))
OK[EXPIRATION] = lambda x: (
    0 if x.get(EXPIRATION_ACTIVE) == ACTIVE_YES else 1,
    0 if x.get(EXPIRATION_EXPIRE) != EXPIRE_NA else 1,
    s2d(x.get(EXPIRATION_EXPIRE)))
OK_REVERSE = {}
OK_REVERSE[CATEGORY] = False
OK_REVERSE[INSTANCE] = False
OK_REVERSE[EVENT] = False
OK_REVERSE[EXPIRATION] = None

### FORMAT ###
CASES_SEPARATOR = '|'
CONDITION_SEPARATOR = ':'
SPACE = ' '
PATH_SEPARATOR = '/'

### IMPORT RETURN CODES ###
IMPORT_OK = 0
IMPORT_WRONG_FILE_FORMAT = 1
IMPORT_NOTHING_TO_IMPORT = 2
