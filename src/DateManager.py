"""
This file contains all variables and function related to Date manipulation.
"""

from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from Configuration import SYSTEM_DATE_FORMAT, LOCAL_DATE_FORMAT, ICS_DATE_FORMAT
from LogManager import logger

today = date.today()
nulldate = datetime(1900, 1, 1).date()

### SYSTEM FUNCTION ###

def date2str(date_from):
    logger.debug(locals())
    if date_from == nulldate:
        logger.debug('Converting a null date to string')
        return ''
    return date_from.strftime(SYSTEM_DATE_FORMAT)

def str2date(str_from):
    logger.debug(locals())
    if str_from == '':
        logger.debug('Converting an empty string to a date')
        return nulldate
    return datetime.strptime(str_from, SYSTEM_DATE_FORMAT).date()

def caldate2str(date_from):
    logger.debug(locals())
    return date2str(datetime(date_from.year, date_from.month+1, date_from.day).date())

def caldate2date(date_from):
    logger.debug(locals())
    return datetime(date_from.year, date_from.month+1, date_from.day).date()

def str2ics_date(str_from):
    logger.debug(locals())
    return datetime.strptime(str_from, SYSTEM_DATE_FORMAT).date().strftime(ICS_DATE_FORMAT)

def str2ics_next_date(str_from):
    logger.debug(locals())
    return (datetime.strptime(str_from, SYSTEM_DATE_FORMAT).date() \
                                 + relativedelta(days=1)).strftime(ICS_DATE_FORMAT)

### L10N FUNCTIONS ###

def local_weekday(date_from):
    logger.debug(locals())
    if isinstance(date_from, str):
        date_from = str2date(date_from)
    return date_from.strftime('%A')

def local_fulldate(date_from):
    logger.debug(locals())
    if isinstance(date_from, str):
        date_from = str2date(date_from)
    return date_from.strftime(LOCAL_DATE_FORMAT)

### DELTA ###

def str2delta(str_from):
    logger.debug(locals())
    nb, dtype = str_from.split(' ')
    logger.debug('nb=' + str(nb))
    logger.debug('dtype=' + str(dtype))
    if dtype == 'days':
        return relativedelta(days=int(nb))
    if dtype == 'weeks':
        return relativedelta(weeks=int(nb))
    if dtype == 'months':
        return relativedelta(months=int(nb))
    if dtype == 'years':
        return relativedelta(years=int(nb))
    logger.error("Could not convert '" + str(str_from) + "' to a delta")
    return None

def get_age(year):
    logger.debug(locals())
    return today.year - int(year)
