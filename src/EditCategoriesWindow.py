import os
from gi.repository import Gtk
from LogManager import logger
import DataFileDefinition as DFD
from Translation import _, l_
from ImageManager import (
    get_icon, get_icon_fn, create_tmp_icon, save_new_icons, discard_new_icons,
    DIR_CATEGORIES)
from TextManager import xstr

### UI objects ###

UI_FILE = 'UI/edit_categories_window.glade'

# Windows #
WINDOW = 'dialog_categories'
WIN_CHOOSE_FILE = 'file_chooser_window'

# TreeViews #
TV_CATEGORIES = 'categories_treeview'
TV_INSTANCES = 'instances_treeview'
TV_EVENTS = 'events_treeview'

# ListStores #
LS_CATEGORIES = 'list_categories'
LS_INSTANCES = 'liststore_instances'
LS_EVENTS = 'list_items'
LS_EXPIRATIONS = 'list_exp_items'

# Category frame #
CATEGORY_TITLE_LABEL = 'cw_category_label'
CATEGORY_ICON = 'category_image'
CATEGORY_SWITCH_BIRTH_YEAR = 'birth_year_switch'

# Categories TreeView #
CATEGORY_CELL_LABEL = 'category_cell_renderer'

# Instance TreeView #
INSTANCE_CELL_LABEL = 'instance_cell_renderer'
INSTANCE_COLUMN_BIRTH_YEAR = 'birth_year_column'

# Misc #
FILTER_CA = 'filter_ca'
FILTER_IMAGE = 'filter_images'

### CONSTANTS ###

DEFAULT_YOB = 1970
LABEL_NOT_SET = '<not set>'

class EditCategoriesWindow:
    def __init__(self, ca, app):
        logger.debug(locals())
        self.ca = ca
        self.app = app
        self._UI = None
        self.loading = False
        self.reordering = False
        self.destroying_changes = None

    ### Main UI functions ###

    def UI(self, name):
        return self._UI.get_object(name)

    def alert(self, title, message):
        logger.debug(locals())
        m = Gtk.MessageDialog(self.UI(WINDOW), Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING,
                              Gtk.ButtonsType.OK, title)
        m.format_secondary_text(message)
        result = m.run()
        m.close()

    def question(self, title, message):
        logger.debug(locals())
        m = Gtk.MessageDialog(self.UI(WINDOW), Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION,
                              Gtk.ButtonsType.OK_CANCEL, title)
        m.format_secondary_text(message)
        m.get_action_area().get_children()[1].get_style_context().add_class('destructive-action')
        result = m.run()
        m.close()
        return result == Gtk.ResponseType.OK

    def save_changes(self):
        logger.debug(locals())
        self.ca.save()
        save_new_icons()

    def discard_changes(self):
        logger.debug(locals())
        self.ca.cancel_changes()
        discard_new_icons()

    def load_window(self):
        logger.debug(locals())
        if self._UI is None:
            self._UI = Gtk.Builder.new_from_file(UI_FILE)
            self._UI.connect_signals(self)

        self.load_categories()
        self.destroying_changes = []

        while True:
            response = self.UI(WINDOW).run()
            if response == Gtk.ResponseType.OK:
                if not self.destroying_changes:
                    self.save_changes()
                    self.UI(WINDOW).hide()
                    return True
                else:
                    destroying_changes_label = \
                        _('Removed objects and all their content would be lost')
                    confirm = self.question(_('Are you sure ?'), destroying_changes_label)
                    if confirm:
                        self.save_changes()
                        self.UI(WINDOW).hide()
                        return True
            else:
                self.discard_changes()
                self.UI(WINDOW).hide()
                return False

    def load_categories(self):
        logger.debug(locals())
        self.loading = True
        self.UI(LS_CATEGORIES).clear()
        categories = self.ca.get_sorted_array(DFD.CATEGORY)
        for category in categories:
            category_id = category.get(DFD.ID[DFD.CATEGORY])
            self.UI(LS_CATEGORIES).append([category_id, get_icon(category_id, DIR_CATEGORIES),
                                           l_(category.get(DFD.LABEL))])
        self.loading = False
        if len(categories) > 0:
            self.UI(TV_CATEGORIES).get_selection().select_iter( \
                self.UI(LS_CATEGORIES).get_iter_first())

    def load_instances(self, path):
        logger.debug(locals())
        self.loading = True
        self.UI(LS_INSTANCES).clear()
        instances = self.ca.get_sorted_array(DFD.INSTANCE, path)
        for instance in instances:
            instance_id = instance.get(DFD.ID[DFD.INSTANCE])
            yob_param = self.ca.get(DFD.PARAM,
                                    DFD.PATH_SEPARATOR.join([path, instance_id,
                                                             DFD.PARAMETER_TYPE_YEAR_BIRTH]))
            yob = DEFAULT_YOB if yob_param is None else int(yob_param.text)
            self.UI(LS_INSTANCES).append([instance_id, l_(instance.get(DFD.LABEL)), yob])
        self.loading = False

    def load_events(self, path):
        logger.debug(locals())
        self.loading = True
        self.UI(LS_EVENTS).clear()
        events = self.ca.get_sorted_array(DFD.EVENT, path)
        for event in events:
            event_id = event.get(DFD.ID[DFD.EVENT])
            self.UI(LS_EVENTS).append([event_id, l_(event.get(DFD.LABEL))])
        self.loading = False

    def load_expirations(self, path):
        logger.debug(locals())
        self.loading = True
        self.UI(LS_EXPIRATIONS).clear()
        events = self.ca.get_sorted_array(DFD.EVENT, path.split(DFD.PATH_SEPARATOR)[0])
        for event in events:
            event_id = event.get(DFD.ID[DFD.EVENT])
            expiration = self.ca.get(DFD.EXPIRATION, DFD.PATH_SEPARATOR.join([path, event_id]))
            self.UI(LS_EXPIRATIONS).append([event_id, (expiration.get(DFD.EXPIRATION_ACTIVE) \
                == DFD.ACTIVE_YES), l_(event.get(DFD.LABEL))])
        self.loading = False

    def show_category(self, path):
        logger.debug(locals())
        category = self.ca.get(DFD.CATEGORY, path)
        label = _(LABEL_NOT_SET) if category.get(DFD.LABEL) is None else l_(category.get(DFD.LABEL))
        self.UI(CATEGORY_TITLE_LABEL).set_text(label)
        self.UI(CATEGORY_ICON).set_from_file(get_icon_fn(category.get(DFD.ID[DFD.CATEGORY]),
                                                         DIR_CATEGORIES))

        has_birth_year_param \
            = self.ca.get(DFD.PARAMETER,
                          DFD.PATH_SEPARATOR.join([path,
                                                   DFD.PARAMETER_TYPE_YEAR_BIRTH])) is not None
        self.UI(CATEGORY_SWITCH_BIRTH_YEAR).set_active(has_birth_year_param)
        self.UI(INSTANCE_COLUMN_BIRTH_YEAR).set_visible(has_birth_year_param)

        self.load_events(path)
        self.load_instances(path)
        self.UI(TV_EVENTS).get_selection().select_path(Gtk.TreePath.new_from_indices([0]))
        self.UI(TV_INSTANCES).get_selection().select_path(Gtk.TreePath.new_from_indices([0]))

    def show_instance(self, path):
        logger.debug(locals())
        self.load_expirations(path)

    ### ACCESSOR ###

    def get_path_for(self, last_part, obj_type=DFD.CATEGORY):
        logger.debug(locals())
        return DFD.PATH_SEPARATOR.join([self.get_path(obj_type), last_part])

    def get_path(self, obj_type=DFD.CATEGORY):
        logger.debug(locals())
        if obj_type == DFD.CATEGORY:
            return self.get_current_category_id()
        if obj_type == DFD.INSTANCE:
            return DFD.PATH_SEPARATOR.join([self.get_current_category_id(),
                                            self.get_current_instance_id()])
        if obj_type == DFD.EVENT:
            return DFD.PATH_SEPARATOR.join([self.get_current_category_id(),
                                            self.get_current_event_id()])
        logger.error('Unknown type '+xstr(obj_type))
        return None

    def get_current_category_position(self):
        logger.debug(locals())
        model, it = self.UI(TV_CATEGORIES).get_selection().get_selected()
        path = model.get_path(it)
        return path.get_indices()[0]

    def get_current_category_id(self):
        logger.debug(locals())
        model, it = self.UI(TV_CATEGORIES).get_selection().get_selected()
        return model.get(it, 0)[0]

    def get_current_instance_id(self):
        logger.debug(locals())
        model, it = self.UI(TV_INSTANCES).get_selection().get_selected()
        return model.get(it, 0)[0]

    def get_current_event_id(self):
        logger.debug(locals())
        model, it = self.UI(TV_EVENTS).get_selection().get_selected()
        return model.get(it, 0)[0]

    ### SIGNALS ###

    # Category #

    def s__add_category(self, button):
        if self.loading or self.reordering: return
        logger.info(locals())
        self.loading = True
        pb = get_icon(None, DIR_CATEGORIES)
        new_id = self.ca.add(DFD.CATEGORY)
        self.UI(LS_CATEGORIES).append([new_id, pb, ''])
        last = self.UI(LS_CATEGORIES).iter_n_children()
        col = self.UI(TV_CATEGORIES).get_column(0)
        self.UI(TV_CATEGORIES).set_cursor_on_cell(Gtk.TreePath.new_from_indices([last - 1]),
                                                  col, self.UI(CATEGORY_CELL_LABEL), True)
        self.loading = False
        self.show_category(new_id)

    def s__remove_category(self, button):
        if self.loading or self.reordering: return
        logger.info(locals())
        self.loading = True
        model, it = self.UI(TV_CATEGORIES).get_selection().get_selected()
        self.destroying_changes.append([DFD.CATEGORY, self.ca.get(DFD.CATEGORY,
                                                                  self.get_path()).get(DFD.LABEL)])
        self.ca.remove(DFD.CATEGORY, self.get_path())
        model.remove(it)
        self.loading = False
        self.show_category(self.get_path())

    def s__category_edited(self, text_rendered, position, new_label):
        if self.loading: return
        logger.info(locals())
        self.UI(CATEGORY_TITLE_LABEL).set_text(new_label)
        self.UI(LS_CATEGORIES)[position][2] = new_label
        self.ca.rename(DFD.CATEGORY, self.UI(LS_CATEGORIES)[position][0], new_label)

    def s__category_changed(self, tree_selection):
        if self.loading or self.reordering: return
        logger.info(locals())
        (model, it) = tree_selection.get_selected()
        path = model.get_value(it, 0)
        self.show_category(path)

    def s__categories_reordered(self, list_store, index):
        if self.loading: return
        logger.info(locals())
        i = 0
        for category in list_store:
            self.ca.update_attr(DFD.CATEGORY, category[0], DFD.POSITION, str(i))
            i += 1
        reorder_from = index.get_indices()[0]
        new_position = self.reorder_to - (0 if reorder_from > self.reorder_to else 1)
        self.UI(TV_CATEGORIES).get_selection().select_path( \
            Gtk.TreePath.new_from_indices([new_position]))
        self.reordering = False

    def s__categories_reordered_before(self, list_store, index, it):
        if self.loading: return
        logger.info(locals())
        self.reordering = True
        self.reorder_to = index.get_indices()[0]

    def s__switch_yob_param(self, switch, position):
        if self.loading: return
        logger.info(locals())
        self.ca.set_yob_param(self.get_path(), position)
        self.UI(CATEGORY_SWITCH_BIRTH_YEAR).set_active(position)
        self.UI(INSTANCE_COLUMN_BIRTH_YEAR).set_visible(position)

    def s__change_icon(self, button):
        logger.info(locals())
        self.UI(WIN_CHOOSE_FILE).set_filter(self.UI(FILTER_IMAGE))
        self.UI(WIN_CHOOSE_FILE).set_current_folder(os.path.expanduser('~'))
        response = self.UI(WIN_CHOOSE_FILE).run()
        if response in [Gtk.ResponseType.OK, Gtk.ResponseType.NONE]:
            filename = self.UI(WIN_CHOOSE_FILE).get_filename()
            create_tmp_icon(filename, self.get_path(), DIR_CATEGORIES)
            self.UI(CATEGORY_ICON).set_from_file(get_icon_fn(self.get_path(), DIR_CATEGORIES))
            self.UI(LS_CATEGORIES)[self.get_current_category_position()][1] \
                = get_icon(self.get_path(), DIR_CATEGORIES)
        self.UI(WIN_CHOOSE_FILE).hide()

    def s__file_selected(self, filename):
        logger.info(locals())
        self.UI(WIN_CHOOSE_FILE).hide()

    def s__import_category(self, button):
        logger.info(locals())
        self.UI(WIN_CHOOSE_FILE).set_filter(self.UI(FILTER_CA))
        self.UI(WIN_CHOOSE_FILE).set_current_folder('data')
        response = self.UI(WIN_CHOOSE_FILE).run()
        if response in [Gtk.ResponseType.OK, Gtk.ResponseType.NONE]:
            filename = self.UI(WIN_CHOOSE_FILE).get_filename()
            result = self.ca.import_xml(filename)
            if result == DFD.IMPORT_WRONG_FILE_FORMAT:
                logger.error('Trying to import an invalid XML file')
                logger.error(locals())
                self.alert(_('Oops'), _('The file you selected is not valid'))
            elif result == DFD.IMPORT_NOTHING_TO_IMPORT:
                logger.warning('Nothing to import from XML file')
                self.alert(_('Nothing imported'),
                           _('The file is whether empty, or the categories already exist'))
            elif result == DFD.IMPORT_OK:
                self.load_categories()
                #TODO : select imported category
                #self.show_category()
            else:
                logger.error('Unknown answer')
                logger.error(locals())
                self.alert(_('Oops'), _('Nothing imported, unknown error.'))
        self.UI(WIN_CHOOSE_FILE).hide()

    # Instance #

    def s__add_instance(self, button):
        if self.loading or self.reordering: return
        logger.info(locals())
        self.loading = True
        last = self.UI(LS_INSTANCES).iter_n_children()
        new_id = self.ca.add(DFD.INSTANCE, self.get_path())
        path = self.get_path_for(new_id)
        self.UI(LS_INSTANCES).append([new_id, '', DEFAULT_YOB])
        col = self.UI(TV_INSTANCES).get_column(0)
        self.show_instance(path)
        self.UI(TV_INSTANCES).set_cursor_on_cell(Gtk.TreePath.new_from_indices([last]),
                                                 col, self.UI(INSTANCE_CELL_LABEL), True)
        self.loading = False

    def s__remove_instance(self, button):
        if self.loading or self.reordering: return
        logger.info(locals())
        self.loading = True
        model, it = self.UI(TV_INSTANCES).get_selection().get_selected()
        self.destroying_changes.append( \
            [DFD.INSTANCE, self.ca.get(DFD.INSTANCE, self.get_path(DFD.INSTANCE)).get(DFD.LABEL)])
        self.ca.remove(DFD.INSTANCE, self.get_path(DFD.INSTANCE))
        model.remove(it)
        self.loading = False

    def s__instance_edited(self, text_rendered, position, new_label):
        if self.loading: return
        logger.info(locals())
        self.UI(LS_INSTANCES)[position][1] = new_label
        self.ca.rename(DFD.INSTANCE, self.get_path_for(self.UI(LS_INSTANCES)[position][0]),
                       new_label)

    def s__instance_year_edited(self, text_rendered, position, new_year):
        logger.info(locals())
        #if the year is valid...
        if int(new_year) > 1900 and int(new_year) < 10000:
            self.UI(LS_INSTANCES)[position][2] = int(new_year)
            self.ca.update_value(DFD.PARAM, self.get_path_for(DFD.PARAMETER_TYPE_YEAR_BIRTH,
                                                              DFD.INSTANCE), str(new_year))

    def s__instance_changed(self, tree_selection):
        if self.loading or self.reordering: return
        logger.info(locals())
        (model, it) = tree_selection.get_selected()
        path = model.get_value(it, 0)
        self.show_instance(self.get_path_for(str(path)))

    def s__instances_reordered(self, list_store, index):
        if self.loading: return
        logger.info(locals())
        i = 0
        for instance in list_store:
            self.ca.update_attr(DFD.INSTANCE, self.get_path_for(instance[0]), DFD.POSITION, str(i))
            i += 1
        reorder_from = index.get_indices()[0]
        new_position = self.reorder_to - (0 if reorder_from > self.reorder_to else 1)
        self.UI(TV_INSTANCES).get_selection().select_path( \
            Gtk.TreePath.new_from_indices([new_position]))
        self.reordering = False

    def s__instances_reordered_before(self, list_store, index, it):
        if self.loading: return
        logger.info(locals())
        self.reordering = True
        self.reorder_to = index.get_indices()[0]

    # Events #

    def s__add_event(self, button):
        logger.info(locals())
        new_id, new_label = self.app.create_new_event(self.get_path(DFD.CATEGORY))
        if new_id is not None:
            self.UI(LS_EVENTS).append([new_id, new_label])
            self.show_instance(self.get_path(DFD.INSTANCE))

    def s__remove_event(self, button):
        logger.info(locals())
        it = self.UI(TV_EVENTS).get_selection().get_selected()[1]
        self.destroying_changes.append( \
            [DFD.EVENT, self.ca.get(DFD.EVENT, self.get_path()).get(DFD.LABEL)])
        self.ca.remove(DFD.EVENT, self.get_path())
        self.UI(LS_EVENTS).remove(it)

    def s__edit_event(self, treeview, row, col):
        logger.info(locals())
        new_label = self.app.edit_event(self.get_path(DFD.EVENT))
        if new_label is not None:
            self.UI(LS_EVENTS)[row][1] = new_label
            self.show_instance(self.get_path(DFD.INSTANCE))

    # Expiration #

    def s__activate_expiration(self, col, row):
        logger.info(locals())
        self.UI(LS_EXPIRATIONS)[row][1] = not self.UI(LS_EXPIRATIONS)[row][1]
        path = self.get_path_for(self.UI(LS_EXPIRATIONS)[row][0], DFD.INSTANCE)
        if self.UI(LS_EXPIRATIONS)[row][1]:
            self.ca.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_ACTIVE, DFD.ACTIVE_YES)
        else:
            self.ca.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_ACTIVE, DFD.ACTIVE_NO)

    def s__unselect_all(self, button):
        logger.info(locals())
        for row in self.UI(LS_EXPIRATIONS):
            row[1] = False
            path = self.get_path_for(row[0], DFD.INSTANCE)
            self.ca.update_attr(DFD.EXPIRATION, path, DFD.EXPIRATION_ACTIVE, DFD.ACTIVE_NO)
