from gi.overrides.Gtk import ComboBox
from gi.repository import Gtk
import DataFileDefinition as DFD
from LogManager import logger
from Translation import l_

### UI objects ###

FILE_MAIN = 'UI/edit_event_window.glade'
FILE_DYNAMICS = 'UI/dynamic_elements.glade'

# Windows #
WINDOW = 'dialog_edit'

# Main elements #
LABEL = 'mw_label'
DESCRIPTION = 'mw_description'
CONDITIONAL_SWITCH = 'mw_conditional'

## Dynamic sections ##

APPROACH = DFD.EVENT_APPROACH
DURATION = DFD.EVENT_DURATION
EXPIRING = DFD.EVENT_EXPIRING
SECTIONS = [APPROACH, DURATION, EXPIRING]
NOTIFICATION = DFD.EVENT_NOTIFICATION

# Revealers #

RV_CONDITIONAL = 'reveal_conditional_#var#'
RV_CONDITIONAL_2 = 'reveal_conditional_#var#_2'
RV_ALWAYS = 'reveal_#var#_always'
RV_IF = 'reveal_#var#_if'
RV_OTHERWISE = 'reveal_#var#_otherwise'
RV_ADDITIONAL = 'reveal_#var#_additional_conditions'

RV_NOTIFICATIONS = 'rev_notifications'

# Form elements #

FORM_MAIN_FIRST = 'mw_#var#_0'
FORM_UNIT_FIRST = 'mw_#var#_0_unit'
FORM_COND_BOX_FIRST = 'mw_#var#_0_cond_box'
FORM_MAIN_LAST = 'mw_#var#_last'
FORM_UNIT_LAST = 'mw_#var#_last_unit'
ADDITIONAL_CONDITIONS_BOX = 'mw_#var#_box'

def widget_is_combobox(widget):
    return isinstance(widget, ComboBox)

class EditEventWindow:
    def __init__(self, ca, app):
        self.ca = ca
        self.app = app
        self._UI = None
        self.nb_conditions = {}
        self.nb_notifications = 0

    def UI(self, name, section=''):
        return self._UI.get_object(name.replace('#var#', section))

    def load_window(self, event=None):
        logger.debug(locals())
        if self._UI is None:
            self._UI = Gtk.Builder.new_from_file(FILE_MAIN)
            self._UI.connect_signals(self)
        self.load(event)
        response = self.UI(WINDOW).run()
        self.UI(WINDOW).hide()
        return response

    ### LOADING ###

    def load(self, event):
        logger.debug(locals())
        self.UI(CONDITIONAL_SWITCH).set_active(False)
        if event is None:
            self.UI(LABEL).set_text('')
            self.UI(DESCRIPTION).get_buffer().set_text('')
            for section in SECTIONS:
                self.load_section(section, '')
            self.load_notifications('')
        else:
            self.UI(LABEL).set_text(l_(event.get(DFD.LABEL)))
            self.UI(DESCRIPTION).get_buffer().set_text(l_(event.get(DFD.EVENT_DESCRIPTION)))
            for section in SECTIONS:
                self.load_section(section, event.get(section))
            self.load_notifications(event.get(DFD.EVENT_NOTIFICATION))

    def load_notifications(self, content):
        logger.debug(locals())
        if content == '':
            self.nb_notifications = 0
            self.update_notifications()
        else:
            content_parts = content.split(DFD.CASES_SEPARATOR)
            self.nb_notifications = len(content_parts)
            self.update_notifications()
            notifications_boxes = self.UI(RV_NOTIFICATIONS).get_child().get_children()
            for i in range(len(content_parts)):
                duration_nb, duration_unit = content_parts[i].split(DFD.SPACE)
                notifications_boxes[i].get_children()[0].set_value(float(duration_nb))
                notifications_boxes[i].get_children()[1].set_active_id(duration_unit)

    def load_sub_section(self, content, widgets, widget_condition_box, first=True):
        logger.debug(locals())
        if content[0] == '':
            if widget_is_combobox(widgets[0]):
                widgets[0].set_active_id(None)
            else:
                widgets[0].set_value(0)
                widgets[1].set_active_id(None)
            return
        if widget_is_combobox(widgets[0]):
            if len(content) == 1:
                widgets[0].set_active_id(content[0])
            else:
                widgets[0].set_active_id(content[1])
                self.set_condition_in_box(widget_condition_box, content[0], 0 if first else 1)
        else:
            if len(content) == 1:
                nb_period, period_unit = content[0].split(DFD.SPACE)
            else:
                nb_period, period_unit = content[1].split(DFD.SPACE)
                self.set_condition_in_box(widget_condition_box, content[0], 0 if first else 2)
            widgets[0].set_value(float(nb_period))
            widgets[1].set_active_id(period_unit)

    def load_section(self, section, content):
        logger.debug(locals())
        content_parts = content.split(DFD.CASES_SEPARATOR)
        nb = len(content_parts)-1
        self.nb_conditions[section] = nb
        self.update_condition_section(section)
        content_ = content_parts[0].split(DFD.CONDITION_SEPARATOR)

        self.load_sub_section(content_, [self.UI(FORM_MAIN_FIRST, section), self.UI(FORM_UNIT_FIRST, section)], self.UI(FORM_COND_BOX_FIRST, section))

        if nb > 0:
            self.UI(CONDITIONAL_SWITCH).set_active(True)
            ac_box_children = self.UI(ADDITIONAL_CONDITIONS_BOX, section).get_children()
            for i in range(1, nb):
                current_box_children = ac_box_children[i-1].get_children()
                content_ = content_parts[i].split(DFD.CONDITION_SEPARATOR)
                self.load_sub_section(content_, current_box_children, ac_box_children[i-1], False)
            content_last_split = content_parts[nb]
            if self.first_element_is_combobox(section):
                self.UI(FORM_MAIN_LAST, section).set_active_id(content_last_split)
            else:
                nb_period, period_unit = content_last_split.split(DFD.SPACE)
                self.UI(FORM_MAIN_LAST, section).set_value(float(nb_period))
                self.UI(FORM_UNIT_LAST, section).set_active_id(period_unit)

    ### MANAGE CONDITION BOXES ###

    def set_condition_in_box(self, box, condition, offset=0):
        logger.debug(locals())
        condition_part = condition.split(DFD.SPACE)
        box.get_children()[1+offset].set_active_id(condition_part[0])
        if len(condition_part) == 1:
            box.get_children()[2+offset].set_reveal_child(False)
        else:
            box.get_children()[2+offset].set_reveal_child(True)
            box.get_children()[2+offset].get_child().get_children()[0].set_active_id(condition_part[1])
            box.get_children()[2+offset].get_child().get_children()[1].set_text(condition_part[2])

    def get_condition_in_box(self, box, offset=0):
        ret = box.get_children()[1+offset].get_active_id()
        if box.get_children()[2+offset].get_reveal_child():
            ret += DFD.SPACE + box.get_children()[2+offset].get_child().get_children()[0].get_active_id()
            ret += DFD.SPACE + box.get_children()[2+offset].get_child().get_children()[1].get_text()
        return ret

    ### INTERNAL FUNCTIONS ###

    def first_element_is_combobox(self, section):
        logger.debug(locals())
        return widget_is_combobox(self.UI(FORM_MAIN_FIRST, section))

    def get_section_value(self, widgets):
        logger.debug(locals())
        if widget_is_combobox(widgets[0]):
            if widgets[0].get_active_id() is None:
                return ''
            return widgets[0].get_active_id()
        if widgets[1].get_active_id() is None:
            return ''
        return str(int(widgets[0].get_value())) + DFD.SPACE + widgets[1].get_active_id()

    ### ACCESSING INFORMATION ###

    def get(self, item):
        logger.debug(locals())
        if item == DFD.LABEL:
            return self.UI(LABEL).get_text()
        if item == DFD.EVENT_DESCRIPTION:
            tb = self.UI(DESCRIPTION).get_buffer()
            return tb.get_text(tb.get_start_iter(), tb.get_end_iter(), False)
        if item in SECTIONS:
            if self.nb_conditions[item] == 0:
                return self.get_section_value([self.UI(FORM_MAIN_FIRST, item), self.UI(FORM_UNIT_FIRST, item)])
            ret = self.get_condition_in_box(self.UI(FORM_COND_BOX_FIRST, item))
            ret += DFD.CONDITION_SEPARATOR + self.get_section_value([self.UI(FORM_MAIN_FIRST, item), self.UI(FORM_UNIT_FIRST, item)])
            ac_box_children = self.UI(ADDITIONAL_CONDITIONS_BOX, item).get_children()
            for i in range(1, self.nb_conditions[item]):
                current_box_children = ac_box_children[i-1].get_children()
                ret += DFD.CASES_SEPARATOR + self.get_condition_in_box(ac_box_children[i-1], 1 if self.first_element_is_combobox(item) else 2)
                ret += DFD.CONDITION_SEPARATOR + self.get_section_value(current_box_children)
            ret += DFD.CASES_SEPARATOR + self.get_section_value([self.UI(FORM_MAIN_LAST, item), self.UI(FORM_UNIT_LAST, item)])
            return ret
        if item == DFD.EVENT_NOTIFICATION:
            if self.nb_notifications == 0:
                return ''
            notifications_box_children = self.UI(RV_NOTIFICATIONS).get_child().get_children()
            ret = str(int(notifications_box_children[0].get_children()[0].get_value()))
            ret += DFD.SPACE + notifications_box_children[0].get_children()[1].get_active_id()
            for i in range(1, len(notifications_box_children)):
                ret += DFD.CASES_SEPARATOR + str(int(notifications_box_children[i].get_children()[0].get_value()))
                ret += DFD.SPACE + notifications_box_children[i].get_children()[1].get_active_id()
            return ret
        logger.error('Unknown item '+item)

    ### NOTIFICATIONS ###

    def update_notifications(self):    
        logger.debug(locals())
        nb = self.nb_notifications
        self.UI(RV_NOTIFICATIONS).set_reveal_child(nb > 0)
        notifications_box = self.UI(RV_NOTIFICATIONS).get_child()
        nb_notifications = len(notifications_box.get_children())
        while nb > nb_notifications:
            builder = Gtk.Builder()
            builder.add_from_file(FILE_DYNAMICS)
            builder.connect_signals(self)
            box = builder.get_object(NOTIFICATION)
            notifications_box.add(box)
            nb_notifications += 1
        while nb < nb_notifications and nb_notifications > 0:
            notifications_box.remove(notifications_box.get_children()[nb_notifications-1])
            nb_notifications -= 1

    def s__add_notification(self, button):
        logger.info(locals())
        self.nb_notifications += 1
        self.update_notifications()

    def s__remove_notification(self, button):
        logger.info(locals())
        self.nb_notifications -= 1
        self.update_notifications()

    ### CONDITIONAL SECTIONS ###

    def s__switch_conditional(self, switch, position):
        logger.info(locals())
        for section in SECTIONS:
            self.UI(RV_CONDITIONAL, section).set_reveal_child(position)
            self.UI(RV_CONDITIONAL_2, section).set_reveal_child(position)

    def update_condition_section(self, section):
        logger.debug(locals())
        nb = self.nb_conditions[section]
        self.UI(RV_ALWAYS, section).set_reveal_child(nb == 0)
        self.UI(RV_IF, section).set_reveal_child(nb > 0)
        self.UI(RV_OTHERWISE, section).set_reveal_child(nb > 0)
        self.UI(RV_ADDITIONAL, section).set_reveal_child(nb > 1)

        additional_conditions_box = self.UI(RV_ADDITIONAL, section).get_child()
        nb_additional_conditions = len(additional_conditions_box.get_children())
        while (nb-1) > nb_additional_conditions:
            builder = Gtk.Builder()
            builder.add_from_file(FILE_DYNAMICS)
            builder.connect_signals(self)
            box = builder.get_object(section)
            additional_conditions_box.add(box)
            nb_additional_conditions += 1
        while (nb-1) < nb_additional_conditions and nb_additional_conditions > 1:
            additional_conditions_box.remove(additional_conditions_box.get_children()[nb_additional_conditions-1])
            nb_additional_conditions -= 1
        
    def add_condition(self, section):
        logger.debug(locals())
        self.nb_conditions[section] += 1
        self.update_condition_section(section)

    def remove_condition(self, section):
        logger.debug(locals())
        self.nb_conditions[section] -= 1
        self.update_condition_section(section)

    def s__add_condition_approach(self, button):
        logger.info(locals())
        self.add_condition(APPROACH)

    def s__add_condition_duration(self, button):
        logger.info(locals())
        self.add_condition(DURATION)

    def s__add_condition_expiring(self, button):
        logger.info(locals())
        self.add_condition(EXPIRING)

    def s__remove_condition_approach(self, button):
        logger.info(locals())
        self.remove_condition(APPROACH)

    def s__remove_condition_duration(self, button):
        logger.info(locals())
        self.remove_condition(DURATION)

    def s__remove_condition_expiring(self, button):
        logger.info(locals())
        self.remove_condition(EXPIRING)

    def s__condition_changed(self, combo_box):
        logger.info(locals())
        children = combo_box.get_parent().get_children()
        children[len(children)-1].set_reveal_child(combo_box.get_active() >= 2)

    def s__entry_icon(self, icon, icon_position, event_button):
        logger.info(locals())
        self.app.show_help(None, 'conditions')

