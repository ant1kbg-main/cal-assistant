import os
import re
from PIL import Image
from gi.repository import GdkPixbuf
from LogManager import logger
from TextManager import xstr
from Configuration import get_local_file

ICON_SIZE = (32, 32)
ICON_OPTIONS = Image.ANTIALIAS

DIR_ICONS = 'icons'
PATH_ICONS = get_local_file(DIR_ICONS)
DIR_CATEGORIES = 'categories'
PATH_CATEGORIES = os.path.join(PATH_ICONS, DIR_CATEGORIES)
EXT = '.png'
EXT_TMP = '.tmp.png'

DEFAULT_ICON = {}
DEFAULT_ICON[''] = 'no_image'
DEFAULT_ICON[DIR_CATEGORIES] = 'default_category'

EXPIRING = 'expiring'
NOT_SET = 'not_set'
OK = 'ok'
BROKEN = 'broken'

def get_icon_fn(icon, directory=None):
    logger.debug(locals())
    full_path = os.path.join(PATH_ICONS, xstr(directory), xstr(icon))
    full_path_default = os.path.join(PATH_ICONS, DEFAULT_ICON[xstr(directory)])
    possible_files = [full_path + EXT_TMP, full_path + EXT, full_path_default + EXT]
    for filename in possible_files:
        if os.path.exists(filename):
            return filename
    logger.error('Could not find icon ' + xstr(icon) + ' in directory ' + xstr(directory))
    return None

def get_icon(icon, directory=None):
    logger.debug(locals())
    filename = get_icon_fn(icon, directory)
    if filename is None:
        return None
    return GdkPixbuf.Pixbuf.new_from_file(filename)

def create_tmp_icon(filename, icon, directory):
    logger.debug(locals())
    new_image = Image.open(filename)
    new_image.thumbnail(ICON_SIZE, ICON_OPTIONS)
    new_image.save(os.path.join(PATH_ICONS, directory, icon + EXT_TMP))

def save_new_icons():
    logger.debug(locals())
    existing_files = os.listdir(PATH_CATEGORIES)
    for filename in existing_files:
        if filename.endswith(EXT_TMP):
            file_from = os.path.join(PATH_CATEGORIES, filename)
            file_to = os.path.join(PATH_CATEGORIES, re.sub(EXT_TMP+'$', EXT, filename))
            os.rename(file_from, file_to)

def discard_new_icons():
    logger.debug(locals())
    existing_files = os.listdir(PATH_CATEGORIES)
    for filename in existing_files:
        if filename.endswith(EXT_TMP):
            os.remove(os.path.join(PATH_CATEGORIES, filename))

ICON_EXPIRING = get_icon(EXPIRING)
ICON_NOT_SET = get_icon(NOT_SET)
ICON_OK = get_icon(OK)
ICON_BROKEN = get_icon(BROKEN)
