import logging

class CustomFormatter(logging.Formatter):

    BLUE = '\x1b[34m'
    YELLOW = '\x1b[33m'
    RED = '\x1b[31m'
    BOLD_RED = '\x1b[31;1m'
    RESET = '\x1b[0m'
    FORMAT = '%(asctime)s %(levelname)-10s (%(filename)-25s:%(lineno)-3s) %(funcName)-30s %(message)s'

    FORMATS = {
        logging.DEBUG: BLUE + FORMAT + RESET,
        logging.INFO: FORMAT,
        logging.WARNING: YELLOW + FORMAT + RESET,
        logging.ERROR: RED + FORMAT + RESET,
        logging.CRITICAL: BOLD_RED + FORMAT + RESET
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        record.levelname = '[%s]' % record.levelname
        record.funcName = '>%s' % record.funcName
        return formatter.format(record)

logger = logging.getLogger()
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setFormatter(CustomFormatter())
logger.addHandler(ch)
