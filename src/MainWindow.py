from gi.repository import Gtk, Pango
from LogManager import logger
from ImageManager import get_icon, DIR_CATEGORIES, ICON_EXPIRING, ICON_NOT_SET, ICON_BROKEN, ICON_OK
from ColorManager import RED
from DateManager import today, nulldate, str2date, caldate2date, local_fulldate, local_weekday
from TextManager import condition2array, duration2array, i as italic
from Translation import _, l_, n_
from Configuration import (
    cfg_get_bool, cfg_get_int, cfg_set, cfg_switch,
    GRP_BY_CATEGORIES, ORDER_ASCENDING, SHOW_NOT_URGENT, SHOW_NOT_ACTIVE,
    SLIDE_POSITION, HEIGHT, WIDTH,
    SHOW_CONFIRMATION_RENEW, SHOW_EMPTY_CATEGORIES, DIRECT_ACCESS_EVENT_EDITION)
import DataFileDefinition as DFD
from CalFileManager import update_all_cals

### UI objects ###

FILE_MAIN = 'UI/main_window.glade'

# Windows and Popovers#
WINDOW = 'main_window'
POPOVER_CALENDAR = 'popover_calendar'
PANED = 'paned'

# Main elements #
TITLE = 'main_window_title'
DESCRIPTION = 'main_window_description'
BOX_APPROACH = 'box_approach'
BOX_DURATION = 'box_duration'
BOX_EXPIRING = 'box_expiring'
BOX_NOTIFICATIONS = 'box_notifications'

BTN_ACTIVATE = 'activate_button'
BTN_DEACTIVATE = 'deactivate_button'
BTN_RENEW = 'renew_button'
BTN_CALENDAR = 'calendar_button'
BTN_EDIT = 'edit_button'
BTN_CANCEL_RENEW = 'btn_cancel_renew'

CALENDAR = 'calendar'
LABEL_TODAY_WEEKDAY = 'today_weekday'
LABEL_TODAY_DATE = 'today_date'

RV_MESSAGE_RENEWED = 'message_renewed'
LABEL_MESSAGE_RENEWED = 'label_message_renewed'

# Options popover #
SWITCH_GRP_BY_CATEGORIES = 'switch_group_by_categories'
BTN_ASCENDING = 'ascending_button'
BTN_DESCENDING = 'descending_button'
SWITCH_SHOW_NOT_URGENT = 'switch_not_urgent'
SWITCH_SHOW_NOT_ACTIVE = 'switch_not_active'

# TreeViews #
TV_MAIN = 'main_view'
COL_CATEGORY_PIC = 'section_picture'

# ListStores #
LS_MAIN = 'main_store'

def ls_main_selectable(treeselection, model, path, current):
    return not cfg_get_bool(GRP_BY_CATEGORIES) or path.get_depth() > 1

class MainWindow:
    def __init__(self, ca, app):
        logger.debug(locals())
        self.ca = ca
        self.app = app
        self.clearing = False
        self.current_item = None
        self.previous_expire_date = None
        self.keep_visible = []
        self._UI = Gtk.Builder.new_from_file(FILE_MAIN)
        self.UI(WINDOW).set_default_size(cfg_get_int(WIDTH), cfg_get_int(HEIGHT))
        self.UI(PANED).set_position(cfg_get_int(SLIDE_POSITION))
        self.UI(TV_MAIN).set_tooltip_column(8)
        self.UI(TV_MAIN).get_selection().set_select_function(ls_main_selectable)
        self.UI(LABEL_TODAY_WEEKDAY).set_text(_('Today: ')+local_weekday(today))
        self.UI(LABEL_TODAY_DATE).set_text(local_fulldate(today))
        self.UI(SWITCH_GRP_BY_CATEGORIES).set_active(cfg_get_bool(GRP_BY_CATEGORIES))
        self.UI(BTN_ASCENDING).set_active(cfg_get_bool(ORDER_ASCENDING))
        self.UI(BTN_ASCENDING).set_sensitive(not cfg_get_bool(ORDER_ASCENDING))
        self.UI(BTN_DESCENDING).set_active(not cfg_get_bool(ORDER_ASCENDING))
        self.UI(BTN_DESCENDING).set_sensitive(cfg_get_bool(ORDER_ASCENDING))
        self.UI(SWITCH_SHOW_NOT_URGENT).set_active(cfg_get_bool(SHOW_NOT_URGENT))
        self.UI(SWITCH_SHOW_NOT_ACTIVE).set_active(cfg_get_bool(SHOW_NOT_ACTIVE))
        self._UI.connect_signals(self)

    ### UI main functions ###

    def UI(self, name):
        logger.debug(locals())
        return self._UI.get_object(name)

    def ui_current_item(self):
        logger.debug(locals())
        (model, it) = self.UI(TV_MAIN).get_selection().get_selected()
        if it is None:
            logger.debug('Current item is None')
            return None
        ret = model.get_value(it, 0)
        logger.debug('Current item is ' + str(ret))
        return ret

    def open(self):
        logger.debug(locals())
        self.UI(WINDOW).set_application(self.app)
        self.UI(WINDOW).show_all()

    def select(self, path):
        logger.debug(locals())
        self.current_item = path
        self.select_current_item()

    ### Updating the interface ###

    def show_label_in_box(self, container_name, label_array, actual_array=None):
        logger.debug(locals())
        for child in self.UI(container_name).get_children():
            self.UI(container_name).remove(child)
        box = Gtk.VBox(False, 0)
        box.get_style_context().add_class('rounded-box')
        if len(label_array) == 1 and label_array[0] == '':
            label = Gtk.Label()
            label.set_markup(italic(_('Not indicated')))
            label.set_alignment(0, 0.5)
            label.set_padding(10, 3)
            box.pack_start(label, False, False, 5)
        else:
            i = 0
            active_found = (actual_array == None)
            for l in label_array:
                if i > 0:
                    separator = Gtk.HSeparator()
                    separator.get_style_context().add_class('separation')
                    box.pack_start(separator, True, True, 0)
                lbox = Gtk.HBox(False, 0)
                label = Gtk.Label()
                label.set_markup(l)
                label.set_alignment(0, 0.5)
                label.set_padding(10, 3)
                lbox.pack_start(label, False, False, 0)
                if actual_array is not None:
                    actual_array_i = actual_array[i].split(DFD.CONDITION_SEPARATOR)
                    if len(actual_array_i) == 2:
                        if not self.ca.conditions_valid(self.current_item, actual_array_i[0]):
                            icon = Gtk.Image.new_from_pixbuf(ICON_BROKEN)
                            icon.set_tooltip_text(_('The condition is not valid'))
                            lbox.pack_end(icon, False, False, 10)
                        elif self.ca.conditions_fulfilled(self.current_item, actual_array_i[0]) \
                            and not active_found:
                            active_found = True
                            icon = Gtk.Image.new_from_pixbuf(ICON_OK)
                            lbox.pack_end(icon, False, False, 10)
                        else:
                            label.get_style_context().add_class('not-active')
                    elif not active_found:
                        active_found = True
                        icon = Gtk.Image.new_from_pixbuf(ICON_OK)
                        lbox.pack_end(icon, False, False, 10)
                    else:
                        label.get_style_context().add_class('not-active')
                box.pack_start(lbox, False, False, 5)
                i = i+1
        box.show_all()
        self.UI(container_name).add(box)
        self.UI(container_name).set_reveal_child(True)

    def show_item(self, path):
        if self.clearing: return
        logger.debug(locals())
        self.current_item = path
        event = self.ca.get(DFD.EVENT, path)
        expiration = self.ca.get(DFD.EXPIRATION, path)
        active = self.ca.is_active(path)
        renewable = self.ca.is_renewable(path)
        approach = self.ca.get_current_approach(path)
        next_date = self.ca.compute_next_date(path)
        expire = str2date(expiration.get(DFD.EXPIRATION_EXPIRE))

        self.show_active(active)
        self.show_renewable(approach, active, renewable, next_date)
        self.show_expire_date(active, expire)
        self.UI(TITLE).set_text(l_(event.get(DFD.LABEL)))
        self.UI(DESCRIPTION).set_markup(l_(event.get(DFD.EVENT_DESCRIPTION)))
        if active:
            self.UI(TITLE).get_style_context().remove_class('not-active')
            self.UI(DESCRIPTION).get_style_context().remove_class('not-active')
        else:
            self.UI(TITLE).get_style_context().add_class('not-active')
            self.UI(DESCRIPTION).get_style_context().add_class('not-active')
        self.show_label_in_box(BOX_APPROACH, condition2array(event.get(DFD.EVENT_APPROACH)),
                               event.get(DFD.EVENT_APPROACH).split(DFD.CASES_SEPARATOR))
        self.show_label_in_box(BOX_DURATION, condition2array(event.get(DFD.EVENT_DURATION)),
                               event.get(DFD.EVENT_DURATION).split(DFD.CASES_SEPARATOR))
        self.show_label_in_box(BOX_EXPIRING, condition2array(event.get(DFD.EVENT_EXPIRING)),
                               event.get(DFD.EVENT_EXPIRING).split(DFD.CASES_SEPARATOR))
        self.show_label_in_box(BOX_NOTIFICATIONS, duration2array(event.get(DFD.EVENT_NOTIFICATION)))

        self.UI(BTN_EDIT).set_visible(cfg_get_bool(DIRECT_ACCESS_EVENT_EDITION))
        self.UI(RV_MESSAGE_RENEWED).set_reveal_child(False)

    def show_active(self, is_active):
        logger.debug(locals())
        if is_active:
            self.UI(BTN_ACTIVATE).hide()
            self.UI(BTN_DEACTIVATE).show()
        else:
            self.UI(BTN_ACTIVATE).show()
            self.UI(BTN_DEACTIVATE).hide()

    def show_renewable(self, approach, is_active, renewable, next_date):
        logger.debug(locals())
        if approach == DFD.APPROACH_ONCE:
            self.UI(BTN_RENEW).set_label(_('End'))
            tooltip = _('This would mark the item as non-active')
        elif approach == DFD.APPROACH_FIXED:
            self.UI(BTN_RENEW).set_label(_('Renew from current expiration'))
            tooltip = _('The item would expire on {date}').format(date=next_date)
        elif approach == DFD.APPROACH_FLEXIBLE:
            self.UI(BTN_RENEW).set_label(_('Renew from today'))
            tooltip = _('The item would expire on {date}').format(date=next_date)
        else:
            logger.error('Unknown approach : ' + str(approach))

        if is_active and renewable:
            self.UI(BTN_RENEW).set_sensitive(True)
            self.UI(BTN_RENEW).set_tooltip_text(tooltip)
        else:
            self.UI(BTN_RENEW).set_sensitive(False)
            self.UI(BTN_RENEW).set_tooltip_text(None)

    def show_expire_date(self, is_active, expire_date):
        logger.debug(locals())
        if is_active:
            self.UI(BTN_CALENDAR).set_sensitive(True)
            if expire_date == nulldate:
                self.UI(BTN_CALENDAR).set_text(_('Not set'))
                self.UI(CALENDAR).select_day(today.day)
                self.UI(CALENDAR).select_month(today.month-1, today.year)
            else:
                remaining_days = (expire_date - today).days
                if remaining_days == 0:
                    self.UI(BTN_CALENDAR).set_text(_('today'))
                elif remaining_days > 0:
                    self.UI(BTN_CALENDAR).set_text(n_('In {day} day', 'In {day} days', remaining_days).format(day=remaining_days))
                else:
                    self.UI(BTN_CALENDAR).set_text(n_('{day} day ago', '{day} days ago', -1*remaining_days).format(day=-1*remaining_days))
                self.update_calendar(expire_date)
        else:
            self.UI(BTN_CALENDAR).set_sensitive(False)
            self.UI(BTN_CALENDAR).set_text(_('Not active'))

    def update_calendar(self, expire_date):
        logger.debug(locals())
        self.UI(CALENDAR).select_day(expire_date.day)
        self.UI(CALENDAR).select_month(expire_date.month-1, expire_date.year)

    ### Populating TreeViews/TreeStores ###

    def add_category_in_main_store(self, category):
        logger.debug(locals())
        cid = category.get(DFD.ID[DFD.CATEGORY])
        exp_icon = None
        cat_icon = get_icon(cid, DIR_CATEGORIES)
        label = l_(category.get(DFD.LABEL))
        remaining_days = None
        weight = Pango.Weight.BOLD
        days_color = None
        days_weight = None
        fulldate = None
        return self.UI(LS_MAIN).append(None, [cid, exp_icon, cat_icon, label, remaining_days,
                                              weight, days_color, days_weight, fulldate])

    def add_expiration_in_main_store(self, parent, expiration):
        logger.debug(locals())
        eid = expiration.get(DFD.ID[DFD.EXPIRATION])
        active = self.ca.is_expiration_active(expiration)
        not_set = self.ca.is_expiration_not_set(expiration)
        instance = self.ca.get_parent(expiration)
        category = self.ca.get_parent(instance)
        path = DFD.PATH_SEPARATOR.join([category.get(DFD.ID[DFD.CATEGORY]),
                                        instance.get(DFD.ID[DFD.INSTANCE]), eid])
        event = self.ca.get(DFD.EVENT, path)
        cat_icon = None
        label = l_(event.get(DFD.LABEL))
        if len(category.findall(DFD.INSTANCE)) > 1:
            label = '[' + l_(instance.get(DFD.LABEL)) + '] ' + label
        days_color = None
        days_weight = Pango.Weight.NORMAL
        weight = Pango.Weight.NORMAL

        if not active:
            exp_icon, remaining_days_label, fulldate_label = None, None, None
            weight = Pango.Weight.LIGHT
        elif not_set:
            exp_icon, remaining_days_label, fulldate_label = ICON_NOT_SET, _('Not set'), None
        else:
            fulldate = expiration.get(DFD.EXPIRATION_EXPIRE)
            fulldate_label = local_fulldate(fulldate)
            remaining_days = (str2date(fulldate) - today).days
            is_expiring = self.ca.is_expiration_expiring(expiration)
            exp_icon = ICON_EXPIRING if is_expiring else None
            if remaining_days > 0:
                remaining_days_label = _('D-{day}').format(day=remaining_days)
            else:
                days_weight = Pango.Weight.BOLD
                if remaining_days == 0:
                    remaining_days_label = _('D-DAY')
                else:
                    days_color = RED
                    remaining_days_label = _('D+{day}').format(day=-1*remaining_days)
        self.UI(LS_MAIN).append(parent, [path, exp_icon, cat_icon, label, remaining_days_label, weight, days_color, days_weight, fulldate_label])
        return path

    def update_main_store(self):
        logger.debug(locals())
        self.clearing = True
        self.UI(LS_MAIN).clear()
        self.clearing = False

        self.UI(COL_CATEGORY_PIC).set_visible(cfg_get_bool(GRP_BY_CATEGORIES))
        exp_filter = lambda x: (((cfg_get_bool(SHOW_NOT_ACTIVE) or x.get(DFD.EXPIRATION_ACTIVE) == DFD.ACTIVE_YES) and (cfg_get_bool(SHOW_NOT_URGENT) or self.ca.is_expiration_expiring(x))) or self.ca.obj2path(x) in self.keep_visible)
        logger.debug('Items to keep visible : '+str(self.keep_visible))
        if cfg_get_bool(GRP_BY_CATEGORIES):
            categories = self.ca.get_sorted_array(DFD.CATEGORY)
            for category in categories:
                parent = self.add_category_in_main_store(category)
                expirations = self.ca.get_sorted_array(DFD.EXPIRATION, category.get(DFD.ID[DFD.CATEGORY]), True, not cfg_get_bool(ORDER_ASCENDING))
                is_empty = True
                for expiration in filter(exp_filter, expirations):
                    path = self.add_expiration_in_main_store(parent, expiration)
                    is_empty = False
                if is_empty and not cfg_get_bool(SHOW_EMPTY_CATEGORIES):
                    self.UI(LS_MAIN).remove(parent)
            self.UI(TV_MAIN).expand_all()
        else:
            expirations = self.ca.get_sorted_array(DFD.EXPIRATION, None, True, not cfg_get_bool(ORDER_ASCENDING))
            for expiration in filter(exp_filter, expirations):
                path = self.add_expiration_in_main_store(None, expiration)
        if not self.select_current_item():
            self.current_item = None
            self.select_current_item()
        update_all_cals(self.ca)

    def select_current_item_iter(self, it):
        logger.debug(locals())
        if it is None:
            return False
        if self.current_item is None or self.UI(LS_MAIN).get_value(it, 0) == self.current_item:
            self.UI(TV_MAIN).get_selection().select_iter(it)
            return True
        return self.select_current_item_iter(self.UI(LS_MAIN).iter_next(it))

    def select_current_item(self):
        logger.debug(locals())
        if cfg_get_bool(GRP_BY_CATEGORIES):
            it_cat = self.UI(LS_MAIN).get_iter_first()
            while it_cat is not None:
                if self.select_current_item_iter(self.UI(LS_MAIN).iter_children(it_cat)):
                    return True
                it_cat = self.UI(LS_MAIN).iter_next(it_cat)
            return False
        it_exp = self.UI(LS_MAIN).get_iter_first()
        return self.select_current_item_iter(it_exp)

    def s__slider_changed(self, panned, rectangle):
        logger.info(locals())
        cfg_set(SLIDE_POSITION, self.UI(PANED).get_position())

    ### Confguration popover ###

    def s__switch_group_by_category(self, button, position):
        logger.info(locals())
        cfg_switch(GRP_BY_CATEGORIES)
        self.update_main_store()

    def s__switch_show_not_urgent(self, button, position):
        logger.info(locals())
        cfg_switch(SHOW_NOT_URGENT)
        self.update_main_store()

    def s__switch_show_not_active(self, button, position):
        logger.info(locals())
        cfg_switch(SHOW_NOT_ACTIVE)
        if not cfg_get_bool(SHOW_NOT_ACTIVE):
            self.keep_visible = []
        self.update_main_store()

    def s__switch_descending(self, button):
        logger.info(locals())
        if cfg_get_bool(ORDER_ASCENDING):
            self.UI(BTN_DESCENDING).set_sensitive(False)
            self.UI(BTN_ASCENDING).set_active(False)
            self.UI(BTN_ASCENDING).set_sensitive(True)
            cfg_switch(ORDER_ASCENDING)
            self.update_main_store()

    def s__switch_ascending(self, button):
        logger.info(locals())
        if not cfg_get_bool(ORDER_ASCENDING):
            self.UI(BTN_ASCENDING).set_sensitive(False)
            self.UI(BTN_DESCENDING).set_active(False)
            self.UI(BTN_DESCENDING).set_sensitive(True)
            cfg_switch(ORDER_ASCENDING)
            self.update_main_store()

    ### Main actions ###

    def s__activate_event(self, button):
        logger.info(locals())
        self.ca.activate(self.current_item)
        self.update_main_store()

    def s__deactivate_event(self, button):
        logger.info(locals())
        self.keep_visible.append(self.current_item)
        self.ca.deactivate(self.current_item)
        self.update_main_store()
        self.show_renewed_message()

    ### Calendar ###

    def s__hide_calendar(self, button):
        logger.info(locals())
        self.UI(POPOVER_CALENDAR).popdown()
        expiration = self.ca.get(DFD.EXPIRATION, self.current_item)
        expire = str2date(expiration.get(DFD.EXPIRATION_EXPIRE))
        self.update_calendar(expire)

    def s__apply_calendar(self, button):
        logger.info(locals())
        self.previous_expire_date = str2date(self.ca.get(DFD.EXPIRATION, self.current_item).get(DFD.EXPIRATION_EXPIRE))
        self.ca.set_new_date(self.current_item, caldate2date(self.UI(CALENDAR).get_date()))
        self.update_main_store()
        self.UI(POPOVER_CALENDAR).popdown()
        self.show_renewed_message()

    def s__calendar_month_changed(self, calendar):
        logger.info(locals())
        calendar.clear_marks()
        if calendar.get_date().year == today.year and calendar.get_date().month == today.month-1:
            calendar.mark_day(today.day)

    def s__show_today(self, button):
        logger.info(locals())
        self.UI(CALENDAR).select_month(today.month-1, today.year)
        self.UI(CALENDAR).select_day(today.day)

    def s__renew(self, button):
        logger.info(locals())
        logger.error(self.current_item)
        exp = self.ca.get(DFD.EXPIRATION, self.current_item)
        logger.error(exp)
        self.previous_expire_date = str2date(exp.get(DFD.EXPIRATION_EXPIRE))
        self.ca.renew(self.current_item)
        if not self.ca.is_active(self.current_item):
            self.keep_visible.append(self.current_item)
        self.update_main_store()
        self.UI(BTN_RENEW).set_sensitive(False)
        self.show_renewed_message()

    ### Renew message ###

    def show_renewed_message(self):
        logger.debug(locals())
        if not cfg_get_bool(SHOW_CONFIRMATION_RENEW):
            logger.debug('Configuration indicates not to show confirmation messages')
            return
        if self.ca.is_active(self.current_item):
            expiration = self.ca.get(DFD.EXPIRATION, self.current_item)
            current_expire = str2date(expiration.get(DFD.EXPIRATION_EXPIRE))
            self.UI(LABEL_MESSAGE_RENEWED).set_text(_('The expiration date has been updated to {new_date}. Previously, it was {old_date}').format(new_date=current_expire, old_date=self.previous_expire_date))
        else:
            self.UI(LABEL_MESSAGE_RENEWED).set_text(_('The expiration has been marked as inactive.'))
        self.UI(RV_MESSAGE_RENEWED).set_reveal_child(True)

    def s__cancel_renew(self, button):
        logger.info(locals())
        self.UI(RV_MESSAGE_RENEWED).set_reveal_child(False)
        if self.ca.is_active(self.current_item):
            logger.debug('Back to previous date ' + str(self.previous_expire_date))
            self.ca.set_new_date(self.current_item, self.previous_expire_date)
        else:
            self.ca.activate(self.current_item)
        self.update_main_store()

    def s__close_renewed_message(self, button):
        logger.info(locals())
        self.UI(RV_MESSAGE_RENEWED).set_reveal_child(False)

    ### Update main window ###

    def s__main_window_select_item(self, tree_selection):
        logger.info(locals())
        (model, pathlist) = tree_selection.get_selected_rows()
        for path in pathlist:
            tree_iter = model.get_iter(path)
            value = model.get_value(tree_iter, 0)
            self.show_item(value)

    def s__update_event(self, button):
        logger.info(locals())
        new_label = self.app.edit_event(self.current_item, True)
        if new_label is not None:
            self.update_main_store()

    def s__open_dialog_categories(self, button):
        logger.info(locals())
        updated = self.app.edit_categories_window.load_window()
        if updated:
            self.update_main_store()

    def close(self):
        logger.debug(locals())
        width, height = self.UI(WINDOW).get_size()
        cfg_set(HEIGHT, height)
        cfg_set(WIDTH, width)

    def s__quit(self, window, event):
        logger.info(locals())
        self.app.quit_app(None, None)
        return True
