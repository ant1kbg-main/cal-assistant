"""
This file contains all variables and function related to Text manipulation.
"""

import random
import string
from xml.sax.saxutils import escape
from DataFileDefinition import CASES_SEPARATOR, CONDITION_SEPARATOR, SPACE
from Translation import _, n_

NB = '{nb} '
UNITS_SINGULAR = {'days':'day', 'weeks':'week', 'months':'month', 'years':'year'}
UNITS_PLURAL = {'days':'days', 'weeks':'weeks', 'months':'months', 'years':'years'}

def xstr(s):
    return '' if s is None else str(s)

def b(txt):
    return '<b>' + txt + '</b>'

def i(txt):
    return '<i>' + txt + '</i>'

def condition2array(cond):
    cases = cond.split(CASES_SEPARATOR)
    n = len(cases)
    if n == 1:
        return [universal_translate(cond)]
    ret = []
    for j in range(n-1):
        condition, case = cases[j].split(CONDITION_SEPARATOR)
        ret.append(universal_translate(case) + ' ' + b(_('if')) + ' ' + _(escape(condition)))
    ret.append(universal_translate(cases[n-1]) + ' ' + b(_('otherwise')))
    return ret

def universal_translate(label):
   if len(label.split(SPACE)) == 2:
       if label.split(SPACE)[0].isnumeric() and label.split(SPACE)[1] in UNITS_SINGULAR:
           return translate_duration(label)
   return _(label)

def translate_duration(dur):
    if dur == '':
        return ''
    nb, unit = dur.split(SPACE)
    return n_(NB+UNITS_SINGULAR[unit], NB+UNITS_PLURAL[unit], nb).format(nb=nb)

def duration2array(dur):
    if dur == '':
        return ['']
    cases = dur.split(CASES_SEPARATOR)
    ret = []
    for case in cases:
        ret.append(translate_duration(case))
    return ret

def random_id():
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(20))
