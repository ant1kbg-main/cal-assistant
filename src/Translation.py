"""
This file contains all variables and function related to Translation.
"""

import locale
import gettext

GT_INSTANCE = gettext.translation('messages', 'locales')
locale.bindtextdomain('messages', 'locales')

def ltranslate(text):
    if text.startswith('_'):
        return translate(text[1:])
    return text

def ntranslate(text_singular, text_plural, n):
    return GT_INSTANCE.ngettext(text_singular, text_plural, n)

def translate(text):
    return GT_INSTANCE.gettext(text)

l_ = ltranslate
n_ = ntranslate
_ = translate
