#!/usr/bin/python3

import os
import sys
os.chdir(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')))
#pylint: disable=wrong-import-position
from Configuration import create_local_dir
create_local_dir()
from Application import Application
#pylint: enable=wrong-import-position

APP = Application()
EXIT_STATUS = APP.run()
sys.exit(EXIT_STATUS)
