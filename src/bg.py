#!/usr/bin/python3

import os
import time
import psutil
import gi
os.chdir(os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')))
gi.require_version('Notify', '0.7')
#pylint: disable=wrong-import-position
from gi.repository import Notify, GLib
from CalendarAssistant import CalendarAssistant
from Configuration import (
    local_dir_exists, cfg_get_bool, cfg_get_str, get_local_file,
    NOTIFICATION_ON_D_DAY, BG)
from DateManager import str2date, today, str2delta
from ImageManager import get_icon
from Translation import _, l_
from LogManager import logger
import DataFileDefinition as DFD
import TodoFileDefinition as TFD
#pylint: enable=wrong-import-position

class App(object):
    def __init__(self):
        Notify.init('Calendar Assistant')
        self.all_notifications = None
        self.show_notifications()

    def app_is_running(self):
        for proc in psutil.process_iter():
            if proc.name() == cfg_get_str('proc_name', 'INTERNAL'):
                return True
        return False

    def show_notification(self, message, path, urgency=Notify.Urgency.LOW):
        n = Notify.Notification.new(message, _("Don't forget it !"))
        n.set_urgency(urgency)
        icon = get_icon('notification')
        n.set_image_from_pixbuf(icon)
        n.add_action('renew', _('Renew'), self.renew, path)
        n.add_action('open', _('Open'), self.open_app, path)
        n.show()
        self.all_notifications.append(n)
        time.sleep(0.5)

    def show_notifications(self):
        logger.info(locals)
        self.all_notifications = []
        ca = CalendarAssistant()
        expirations = ca.get_sorted_array(DFD.EXPIRATION, None, True)
        for expiration in expirations:
            if expiration.get(DFD.EXPIRATION_EXPIRE) == DFD.EXPIRE_NA \
                or expiration.get(DFD.EXPIRATION_ACTIVE) == DFD.ACTIVE_NO:
                continue
            eid = expiration.get(DFD.ID[DFD.EXPIRATION])
            instance = ca.get_parent(expiration)
            category = ca.get_parent(instance)
            path = DFD.PATH_SEPARATOR.join([category.get(DFD.ID[DFD.CATEGORY]),
                                            instance.get(DFD.ID[DFD.INSTANCE]), eid])
            event = ca.get(DFD.EVENT, path)
            remaining_days = (str2date(expiration.get(DFD.EXPIRATION_EXPIRE)) - today).days

            label = l_(event.get(DFD.LABEL))
            if len(category.findall(DFD.INSTANCE)) > 1:
                label = '[' + l_(instance.get(DFD.LABEL)) + '] ' + label
            if remaining_days == 0 and cfg_get_bool(NOTIFICATION_ON_D_DAY, BG):
                final_label = _('{label} is expiring today !').format(label=label)
                self.show_notification(final_label, path, Notify.Urgency.NORMAL)
            else:
                if event.get(DFD.EVENT_NOTIFICATION) != '':
                    notifications_array \
                        = event.get(DFD.EVENT_NOTIFICATION).split(DFD.CASES_SEPARATOR)
                    for notification in notifications_array:
                        delta = str2delta(notification).days
                        if remaining_days == delta:
                            initial_label = _('{label} is expiring in {nb} days !')
                            final_label = initial_label.format(label=label, nb=str(remaining_days))
                            self.show_notification(final_label, path)
                            break
        GLib.timeout_add_seconds(60*60*24, self.show_notifications)

    def launch_app(self):
        logger.info(locals)
        os.system('cal-assistant')

    def open_app(self, notification, action_name, data):
        logger.info(locals)
        with open(get_local_file(TFD.FILENAME), 'w') as f:
            f.write(TFD.SELECT + TFD.SEPARATOR + data + TFD.EOL)
        self.launch_app()

    def renew(self, notification, action_name, data):
        logger.info(locals())
        if self.app_is_running():
            with open(get_local_file(TFD.FILENAME), 'w') as f:
                f.write(TFD.RENEW + TFD.SEPARATOR + data + TFD.EOL)
            self.launch_app()
        else:
            ca = CalendarAssistant()
            ca.renew(data)

if local_dir_exists():
    app = App()
    GLib.MainLoop().run()
