#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DIR=${SCRIPT_DIR}/..

pushd ${DIR} 
VERSION=$(xmllint --xpath '/interface/object[@id="about_dialog"]/property[@name="version"]/text()' UI/main.glade)
sed -i "s/Version:.*/Version: $VERSION/" packages/cal-assistant-deb/DEBIAN/control

rm -rf packages/cal-assistant-deb/usr/*
rm -rf packages/cal-assistant-deb/etc/*
mkdir -p packages/cal-assistant-deb/usr/bin
mkdir -p packages/cal-assistant-deb/usr/share/cal-assistant/src
mkdir -p packages/cal-assistant-deb/etc/xdg

cp -r doc packages/cal-assistant-deb/usr/share/cal-assistant/doc
cp src/*.py packages/cal-assistant-deb/usr/share/cal-assistant/src/
cp -r UI packages/cal-assistant-deb/usr/share/cal-assistant/UI
cp -r icons packages/cal-assistant-deb/usr/share/cal-assistant/icons
cp -r locales packages/cal-assistant-deb/usr/share/cal-assistant/locales
cp -r applications packages/cal-assistant-deb/usr/share/applications
cp -r autostart packages/cal-assistant-deb/etc/xdg/autostart
cp configuration.cfg packages/cal-assistant-deb/usr/share/cal-assistant/
cp ca.xml packages/cal-assistant-deb/usr/share/cal-assistant/
cp -r data packages/cal-assistant-deb/usr/share/cal-assistant/

pushd ${DIR}/packages/cal-assistant-deb/usr/bin
ln -s ../share/cal-assistant/src/__init__.py cal-assistant
ln -s ../share/cal-assistant/src/bg.py cal-assistant-bg
popd

chown root:root -R packages/cal-assistant-deb
chmod 755 -R packages/cal-assistant-deb

dpkg-deb --build packages/cal-assistant-deb

popd
exit 0
