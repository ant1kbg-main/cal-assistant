#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DIR=${SCRIPT_DIR}/..

pushd ${DIR}/locales/
for d in */; do
	msgfmt ${d}/LC_MESSAGES/messages.po -o ${d}/LC_MESSAGES/messages.mo
done
popd

