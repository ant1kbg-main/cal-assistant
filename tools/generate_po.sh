#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DIR=${SCRIPT_DIR}/..

#UI
pushd ${DIR}/UI > /dev/null
for f in *.glade; do
	intltool-extract --type 'gettext/glade' $f
done
for f in *.h; do
	xgettext --omit-header -kN_ -o ${f}.pot $f
	rm $f
done
msgcat *.pot > ui.pot
mv ui.pot ${DIR}/locales/
rm *.pot
popd > /dev/null

#SRC
pushd ${DIR}/src > /dev/null
for f in *.py; do
	xgettext --omit-header -k_ -o ${f}.pot $f
	xgettext --omit-header -kn_:1,2 -o ${f}_n.pot $f
done
msgcat *.pot > src.pot
mv src.pot ${DIR}/locales/
rm *.pot
popd > /dev/null

#DATA
pushd ${DIR}/data > /dev/null
for f in $(find -name '*.xml'); do
	cat ${DIR}/tools/header.pot > ${f}.pot
	grep -noP '(?<="_)[^"]*' ${f} | \
	while read l; do 
		printf '# %s:%i\n' ${f} $(echo ${l} | cut -d ':' -f1) >> ${f}.pot
		printf 'msgid "%s"\n' "$(echo ${l} | cut -d ':' -f2-)" >> ${f}.pot
		printf 'msgstr ""\n\n' >> ${f}.pot
	done
	msguniq ${f}.pot -o ${f}.pot
done
msgcat *.pot */*.pot > data.pot
mv data.pot ${DIR}/locales/
rm *.pot */*.pot
popd > /dev/null

#finally
pushd ${DIR}/locales/ > /dev/null
msgcat *.pot ${DIR}/tools/manual.pot > messages.pot
for d in */; do
	msgmerge -U ${d}/LC_MESSAGES/messages.po messages.pot
done
popd > /dev/null

