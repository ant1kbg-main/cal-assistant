#!/bin/bash

DIR=~/Travail/CalendarAssistant

GREEN='\x1b[32m'
RED='\x1b[31m'
YELLOW='\x1b[33m'
NC='\x1b[0m'

#pylint
printf -- '-------------------------------------------------------------------------------\n'
pushd ${DIR}/src > /dev/null
for f in *.py; do
	full_rate=$(pylint $f 2> /dev/null | tail -2 | head -1 | cut -d ' ' -f7)
	rate=$(echo $full_rate | cut -d '/' -f1)
	if [[ $rate == 10.00 ]]; then
		printf "%25.25s : %8.8s                                  [  ${GREEN}O-K${NC}  ]\n" $f $full_rate
	elif [[ $rate < 7 ]]; then
		printf "%25.25s : %8.8s                                  [ ${RED}ERROR${NC} ]\n" $f $full_rate
	else
		printf "%25.25s : %8.8s                                  [${YELLOW}WARNING${NC}]\n" $f $full_rate
	fi
done
popd > /dev/null

printf -- '-------------------------------------------------------------------------------\n'
errors=$(pylint -E ${DIR}/src/*.py 2> /dev/null | wc -l)
if [[ $errors == 0 ]]; then
	printf "            pylint errors : %3.3s                                       [  ${GREEN}O-K${NC}  ]\n" $errors
else
	printf "            pylint errors : %3.3s                                       [ ${RED}ERROR${NC} ]\n" $errors
	pylint -E ${DIR}/src/*.py 2> /dev/null
fi

#postats
printf -- '-------------------------------------------------------------------------------\n'
pushd ${DIR}/locales > /dev/null
	for d in */; do
		rate=$(postats ${d}/LC_MESSAGES/messages.po | head -1 | cut -d '(' -f2 | cut -d '%' -f1 | xargs)
		if [[ $rate == 100 ]]; then
			printf "Translation %13.13s : %3.3s%%                                      [  ${GREEN}O-K${NC}  ]\n" $d $rate
		elif [[ $rate < 80 ]]; then
			printf "Translation %13.13s : %3.3s%%                                      [ ${RED}ERROR${NC} ]\n" $d $rate
		else
			printf "Translation %13.13s : %3.3s%%                                      [${YELLOW}WARNING${NC}]\n" $d $rate
		fi
	done
popd > /dev/null

#cloc
cloc ${DIR} | sed '1,5d'
